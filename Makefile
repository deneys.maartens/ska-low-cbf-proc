# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag
PROJECT = ska-low-cbf-proc

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-low-cbf-proc

# Kube app defaults to project name, but we use "ska-low-cbf"
# (to facilitate integration testing in the ska-low-cbf repo)
KUBE_APP = ska-low-cbf

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART ?= test-parent
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/
# Chart for testing
K8S_CHART ?= test-parent
K8S_CHARTS = $(K8S_CHART)

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400
# Helm version
HELM_VERSION = v3.3.1
# kubectl version
KUBERNETES_VERSION = v1.19.2

CI_PROJECT_DIR ?= .

KUBE_CONFIG_BASE64 ?=  ## base64 encoded kubectl credentials for KUBECONFIG
KUBECONFIG ?= /etc/deploy/config ## KUBECONFIG location

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
WEBJIVE ?= false# Enable Webjive
MINIKUBE ?= true ## Minikube or not
ITANGO_ENABLED ?= false

CI_PROJECT_PATH_SLUG ?= ska-low-cbf-proc
CI_ENVIRONMENT_SLUG ?= ska-low-cbf-proc

# TODO - check if this is correct?
OCI_IMAGES ?= ska-low-cbf-proc-ami ska-low-cbf-proc-xrt
OCI_IMAGE_BUILD_CONTEXT ?= $(PWD)
#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/release.mk
include .make/k8s.mk
include .make/make.mk
include .make/python.mk
include .make/helm.mk
include .make/oci.mk
include .make/help.mk
include .make/docs.mk


K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-tango-images-tango-itango:9.3.7 ## TODO: UGUR docker image that will be run for testing purpose
CI_JOB_ID ?= local##pipeline job id
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test

# define private overrides for above variables in here
-include PrivateRules.mak


ITANGO_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.7

PYTHON_VARS_AFTER_PYTEST = -m 'not post_deployment'

HELM_CHARTS_TO_PUBLISH = ska-low-cbf-proc

PYTHON_BUILD_TYPE = non_tag_setup

PYTHON_SWITCHES_FOR_FLAKE8=

PYTHON_SWITCHES_FOR_PYLINT=--fail-under=7

# Add the ability to specify a VALUES_FILE at runtime
ifneq ($(VALUES_FILE),)
CUSTOM_VALUES := --values $(VALUES_FILE)
else
endif

ifneq ($(CI_REGISTRY),)
K8S_TEST_TANGO_IMAGE = --set ska-low-cbf-proc.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set ska-low-cbf-proc.image.registry=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf-proc
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/low-cbf/ska-low-cbf-proc/ska-low-cbf-proc:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
K8S_TEST_TANGO_IMAGE = --set ska-low-cbf-proc.image.tag=$(VERSION)
K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-low-cbf-proc:$(VERSION)
endif

TARANTA_PARAMS = --set ska-taranta.enabled=$(TARANTA) \
				 --set ska-taranta-auth.enabled=$(TARANTA) \
				 --set ska-dashboard-repo.enabled=$(TARANTA)

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_HOST) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
    --set ska-tango-base.itango.enabled=$(ITANGO_ENABLED) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	$(TARANTA_PARAMS) \
	${K8S_TEST_TANGO_IMAGE} \
	${CUSTOM_VALUES}

# Pytango image version - used in this Makefile for 'pipeline_unit_test'
# (formerly used in the base for our container image)
PYTANGO_BUILDER_VERSION=9.3.32

# inject the XRT environment settings for the test containers
PYTHON_VARS_BEFORE_PYTEST=source /opt/xilinx/xrt/setup.sh;

# override python.mk python-pre-test target
python-pre-test:
	@echo "python-pre-test: running with: $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) pytest $(PYTHON_VARS_AFTER_PYTEST) \
	--cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml $(PYTHON_TEST_FILE)"

# workaround for bug in CI templates
python-pre-publish:
	pip3 install twine

k8s-pre-install-chart:
	$(shell echo -e 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gilab_values.yaml)

k8s-pre-template-chart: k8s-pre-install-chart

pipeline_unit_test: ## Run simulation mode unit tests in a docker container as in the gitlab pipeline
	@$(OCI_BUILDER) run --rm --volume="$$(pwd):/app:rw" \
		registry.gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc/pytango-xrt:$(PYTANGO_BUILDER_VERSION) \
		bash -c "make python-test; chown -R $(shell id -u):$(shell id -g) . 2> /dev/null"
# above script "chown"s everything at the end so user can delete test reports without sudo
# chown errors are discarded - it appears sometimes it rejects invalid UIDs
# but also in that case the files have appropriate ownership in the host system?
# possibly a podman vs docker thing

start_pogo: ## start the pogo application in a docker container; be sure to have the DISPLAY and XAUTHORITY variable not empty.
	docker run --network host --user $(shell id -u):$(shell id -g) --volume="$(PWD):/home/tango/ska-low-cbf-proc" --volume="$(HOME)/.Xauthority:/home/tango/.Xauthority:rw" --env="DISPLAY=$(DISPLAY)" $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-pogo:9.6.32

.PHONY: pipeline_unit_test
