# Low.CBF Processor Tango Device #
The Low.CBF Processor TANGO device is used to control & monitor an Alveo FPGA card.

## Documentation
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-proc/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-proc/en/latest/?badge=latest)

The documentation for this project can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf-proc documentation](https://developer.skatelescope.org/projects/ska-low-cbf-proc/en/latest/index.html "SKA Developer Portal: ska-low-cbf-proc documentation")

## Project Avatar (Repository Icon)
[Processor icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/processor "processor icons")

# Directory Structure
Source code files are divided into subdirectories:
 - src/ska_low_cbf_proc - module root - utility / miscellaneous
    - icl - Instrument Control (Abstraction) Layer common
      - correlator - Correlator personality ICL
      - pst - PulSar Timing personality ICL
    - processor - Processor Tango device

# Changelog #

## Unreleased changes ##

### 0.12.0
* Updated Correlator VCT address to handle firmware version >= 0.0.5
* Rename existing container image to `ska-low-cbf-proc-xrt`, add new container image
`ska-low-cbf-proc-ami` (different FPGA drivers)
* Update `processor-device.sh` (container entrypoint script) to detect serial from FPGA
using AMI driver - **Warning** does not cater for multiple FPGAs being present!
* Modify Helm chart structure, image parameters now under `ska-low-cbf-proc.image`
instead of `ska-low-cbf-proc.proc.image`
* added code to program PST packetiser incl metadata (needs PST FPGA built on/after 2024-01-10,
 eg gitlab 0.0.20-dev.7c13dd33)
* Allow firmware download source to be specified in request from Allocator:
`personality[:version[:source]]`
* **Breaking change**: Download firmware from CAR by default!
* Fix [SKB-282](https://jira.skatelescope.org/browse/SKB-282) - fault in handling
`internal_alveo` requesting FPGA to be programmed before Processor starts up
* monitor polynomial delay subscripton mechanism for failures; add subarray-beam delay polynomial validity summary to EDA; add a tango attribute summarising validity of all polynomial delays relevant for a particular subarray
* Station-beam delays programmed. Requires with hardware station-beam-delay-poly evaluation (built on/after 2024-02-13)
* Requires FPGA built on/after 2024-02-24. VCT table moved into VCSTATS RAM to fix register write bug
* **Breaking change** Update processor to use new TM DelayPolynomial format. (https://gitlab.com/ska-telescope/ska-telmodel/-/blob/master/src/ska_telmodel/csp/examples.py?ref_type=heads#L1878-1914)

### 0.11.1
* Update `ska-low-cbf-fpga` to v0.18.1
* Use Helm chart version as default container image tag

## 0.11.0
* Update SPEAD initialisation packet `FreqHz` calculation
* Remove unused `ska-tango-testing` dependency
* Replace `ska_tango_base.control_model` with `ska_control_model`
* Correlator ICL: Add new SPEAD output config registers for base frequency & increment
* Added code supporting firmware with "latch-on" and "reset" regiser changes
  - will not work with firmware built before 2023-09-15
* Update to ska-tango-base v0.18.1
* Add StartRegisterLog command, to log register read/writes to a text file.
**Warning** these messages will also go to the Tango logger and can crash k9s.
* Enabled FPGA image caching when "CACHE_DIR" environment variable is defined
* Added calculation and update of delay register contents from Delay Polynomials (for Correlator FPGAs only at present)
* updated documentation build process because of read-the-docs changes
* Changed internal interface to Allocator (not backward compatible)
* Initial changes made to support PST FPGA
  - FPGA loads and generates PST output
  - waiting on FPGA updates to delay logic and packetiser before making further changes to get fully functioning PST beamforming

## 0.10.0
* Processor Tango device starts up with simulationMode=False if FPGA present.
* simulationMode attribute is now read-only. FPGA Hardware is used if possible,
or simulation can be requested via SelectPersonality command.
* Update to ska-low-cbf-sw-cnic v0.5.0 (duplex, virtual digitiser)
* Update to ska-low-cbf-fpga v0.17.0
* Add multi-personality (CNIC, Correlator, PST) debug console
* Fix [FPGA personalities switching problem](https://jira.skatelescope.org/browse/PERENTIE-1627)
* Update Tango device so it can specify which Xilinx platform (shell) version to use
  when [selecting personality](https://jira.skatelescope.org/browse/PERENTIE-1736)
* Remove AlveoDevice, obsolete Tango device formerly used for FPGA health monitoring
* Switch to Xilinx platform (shell) version 3 (XDMA 3); by default deploy 3 processor instances (e.g. 2\*CNIC, 1 PST)
* Add SPEAD initial packet template generation for Correlator FPGA personality
* Add StartVirtualDigitiser command
* Add `fpga_temperature` and `fpga_power` Tango attributes to monitor FPGA conditions
* Remove `CNIC` due to Poetry dependency issues; Use `CNIC-VD` instead
## 0.9.0
* Add SelectPersonality command with ability to specify firmware source (GitLab,
Nexus Central Artefact Repository)
* Adopt poetry & pyproject.toml instead of setup.py, requirements.txt, etc
* Update to ska-tango-base v0.14.0
  * refactor the AlveoDevice and ProcessorDevice to use the new poller in ska-tango-base
  * using ska-tango-testing for asynchronous testing
* Reinstate simulationMode attribute (was lost during some base class update)
* Implement CallMethod to generically call FPGA specific methods
* Expand Tango device instances in Helm chart to 10 (to match Low PSI capacity)
## 0.8.0
* Update to ska-tango-base v0.13.0
* Fix simulationMode detection logic, update simulationMode documentation
### 0.7.4
* Update container to use Ubuntu 20.04 & XRT 202120.2.12.427
### 0.7.3
* LowCbfProcessor: Update to suit ska-low-cbf-fpga v0.10.0 (change
to how user\_attributes works)
### 0.7.2
* LowCbfProcessor: added StartupSimulationMode property
* processor-device.sh: Disabled (commented-out) firmware downloading
  * I think this was causing problems in CI, and it would be better done inside
the Python code (later)
### 0.7.1
* Use Makefiles from
[ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile)
* Use CI config from
[templates-repository](https://gitlab.com/ska-telescope/templates-repository)
* Rename Kubernetes "app" label to ska-low-cbf to help run integration tests
## 0.7.0
* Common FPGA interface code moved to new repo
[ska-low-cbf-fpga](https://gitlab.com/ska-telescope/ska-low-cbf-fpga)
* LowCbfProcessor & tests updated to use new ska\_low\_cbf\_fpga package
  * LowCbfProcessor simulationMode attribute can be used to selected between
ArgsCl ands ArgsSimulator FPGA drivers
## 0.6.0
* ArgsXrt created, a new ArgsFpgaDriver that works via the Xilinx XRT Python
bindinds, [pyxrt](https://xilinx.github.io/XRT/master/html/pyxrt.html).
### 0.5.4
* ArgsCl: Add support for read & write of arrays larger than exchange buffer
### 0.5.3
* IclField: Add operators and integer typecast
### 0.5.2
* Fix tests that broke in 0.5.1:
  * fpga\_icl.py: Rename imports so type checks work when testing
  * tests/conftest.py: Update patching of args\_map
  ("_load_map" function renamed to "load\_map")
  * tests/test\_packetiser.py: Fix data array offset calculation,
  check IP address value as integer
### 0.5.1
* ArgsSimulator added, an FPGA simulation driver
* ArgsPolledAdapter: Make polling loop do reads in blocks
* IclFpgaField: Fix addressing of elements within value arrays
## 0.5.0
* Major refactor. AlveoCL split into smaller, more modular, pieces: ArgsMap, ArgsCl,
ArgsFpgaDriver etc. (Tango device not yet updated)

### 0.4.14
* Split image build into two parts to avoid slow rebuild of dependencies every
time our code changes
### 0.4.13
* Packetiser: add 'defaults' attribute (use default Ethernet headers)
* PstFpga: expose system attributes for firmware version/personality and Ethernet
debugging
* ska-low-cbf-proc Helm chart: add healthState attribute polling configuration
* Remove EngageSKA URLs from Python requirements
### 0.4.12
* PstFpga: Don't try to access ethernet statistics registers when in simulation
mode (they are not simulated by AlveoCL)
### 0.4.11
* LowCbfProcessor: Modify memory buffers used to communicate with FPGA

*Note for future improvement: the memory configuration should be stored in the
 Tango DB (per firmware image), not hard-coded.*
### 0.4.10
* AlveoCL: Update cached values on write (allows consecutive updates to
bitfields to work as expected)
* Add tests for AlveoCL, ICL, Packetiser
### 0.4.9
* Packetiser peripheral interface bug fix
### 0.4.8
* Packetiser peripheral interface bug fix
### 0.4.7
* Update ICL to control PSR packet generation, and Tango interface to use it
### 0.4.6
* Make "DummyCore" (simulated AlveoCL interface) work for testing without FPGA
* Fix register writes via ICL FPGA interface
### 0.4.5
* Bug fixes relating to conversion to ICL FPGA interface
* LowCbfProcessor now reports its own version number
(instead of ska-tango-base's)
### 0.4.4
* Download firmware image into container at runtime
(from a temporary development server)
### 0.4.3
* Fix mistake in setup.py
### 0.4.2
* Use local imports in Python files
* Sleep in startup script (to allow testing while code is broken)
### 0.4.1
* Fix processor-device.sh (was trying to run LowCbfFpga,
but it's now LowCbfProcessor)
## 0.4.0
* FPGA register interface Tango device (formerly known as LowCbfFpga)
renamed to LowCbfProcessor
* LowCbfProcessor modified to use ICL (Instrument Control Layer)
for PST FPGA firmware personality

### 0.3.4
* Charts, & scripts updated to run Tango devices using
ska-tango-util 0.2.14 & ska-tango-base 0.2.23
* Dockerfile downloads XRT 2.8.726 direct from Xilinx
(previously relied on having the .deb file locally)
### 0.3.3
* Low.CBF FPGA Tango devices split from ska-low-cbf repository.
