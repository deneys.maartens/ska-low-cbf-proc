# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
Command Line Interface for debugging using the various ICL personality classes.
"""

from ska_low_cbf_fpga.fpga_cmdline import FpgaCmdline

from ska_low_cbf_proc.processor.corr_fpga import CorrFpga
from ska_low_cbf_proc.processor.pst_fpga import PstFpga


def main():
    personality_map = {
        "CORR": CorrFpga,  # new correlator ID
        " COR": CorrFpga,  # for backward compatibility with old ID
        " PST": PstFpga,
    }
    FpgaCmdline(personality_map=personality_map)


if __name__ == "__main__":
    main()
