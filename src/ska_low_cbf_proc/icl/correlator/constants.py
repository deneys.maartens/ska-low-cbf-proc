# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
Constants relevant to the Correlator.
"""

SEC_PER_FILTERBANK_SAMPL = 4096 * 1080e-9
"""Correlator filterbank output sample period (seconds)"""
FINE_CHANNEL_HZ = (1 / 1080e-9) * (8 / 32768)  # 226.056 134 259 259...
"""Correlator filterbank channel bandwidth (Hz)."""
FINE_CH_PER_STANDARD_VIS_CH = 24
"""Correlator fine channels accumulated per visibility channel, standard correlation."""

SKA_COARSE_SPACING_GHZ = 0.00078125
SKA_COARSE_SPACING_HZ = int(SKA_COARSE_SPACING_GHZ * 1_000_000_000)
"""SPS channel spacing."""
