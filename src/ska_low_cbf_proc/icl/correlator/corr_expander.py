# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Calculations of correlator register values
"""


import math

FINE_PER_COARSE = 3456
CT2_BYTES_PER_STN_CHNL = (3456 * 6 * 512) // 4
CT2_BYTES_PER_STN_FINE_CHAN = (6 * 512) // 4
CT2_MX_MEM_BYTES = 3 * 512 * 1024 * 1024
NUM_MXC = 2  # number of matrix correlators
SUBARRAY_BEAM_ENTRIES_PER_MXC = 128


def _vct_sort_ordering(vct_entry):
    """Function used to determine VCT entry ordering"""
    if vct_entry is None:
        return 0x80000000
    (sbary, freq, beam, stn, sstn, _) = vct_entry
    key = (sbary & 0x1F) << 26  # 5-bit subarray_id
    key += (freq & 0x01FF) << 17  # 9-bit freq_id
    key += (beam & 0x0F) << 13  # 4-bit beam_id
    key += (stn & 0x03FF) << 3  # 10-bit station_id
    key += sstn & 0x07  # 3-bit sub-station
    return key


def get_words_vct(xpanded_regs) -> list:
    """
    Calculate the uint32_t values that go in the Virtual Channel Table (VCT)
    """
    hexvals = []
    for sub_array, frq_id, stn_bm, stn, substn, vchan in xpanded_regs["vct"]:
        word1 = (
            0x80000000
            + ((sub_array & 0x1F) << 26)
            + ((frq_id & 0x01FF) << 17)
            + ((stn_bm & 0x0F) << 13)
            + ((stn & 0x03FF) << 3)
            + (substn & 0x07)
        )
        word2 = vchan
        hexvals.append(word1)
        hexvals.append(word2)
    # add "invalid entries" to bring size to "total_channels" value because
    # total_channels determines both VCT search window and number of
    # 4-station-blocks read out of 1st corner turn. Latter is greater than
    # former when the number of stations in a subarray is not multiple of 4
    while len(hexvals) < (2 * xpanded_regs["tot_ch"]):
        hexvals.extend([0x00000000, 0x00000000])
    return hexvals


def get_words_demap(xpanded_regs) -> list:
    """
    Calculate the uint32_t values that go in the VC_DEMAP table
    """
    hexvals = []
    for frq_id, blk_no, sbt_idx in xpanded_regs["vc_dmap"]:
        word1 = (
            0x80000000
            + ((frq_id & 0x1FF) << 20)
            + ((blk_no & 0x0FFF) << 8)
            + (sbt_idx & 0x0FF)
        )
        word2 = 0x00000000
        hexvals.append(word1)
        hexvals.append(word2)
    return hexvals


def get_words_subarray_beams(xp_regs, pad: bool = True) -> list:
    """
    Calculate the uint32_t values that go in the SUBARRAY_BEAMS table
    """
    # subarray_beam table is 4 words per entry
    hexvals = []
    for mxc in range(0, NUM_MXC):
        for (
            hbm_base_addr,
            first_coarse,
            first_fine,
            num_fin,
            num_stn,
            itime,
            ichan,
            _,  # num_coarse,
            _,  # subarray,
            _,  # stn_bm,
        ) in xp_regs["sub_bm_tbl"][mxc]:
            word1 = ((first_coarse & 0x0FFFF) << 16) + (num_stn & 0x0FFFF)
            word2 = first_fine
            word3 = 0x0
            if itime == 192:
                word3 = 0x40000000
            word3 += ((ichan & 0x03F) << 24) + (num_fin & 0x0FFFFFF)
            word4 = hbm_base_addr
            hexvals.extend([word1, word2, word3, word4])
        # Fill in unused subarray_beam entries. Without padding the values
        # are not suitable for programming FPGA if the second MxC is in use
        if pad:
            for _ in range(
                len(xp_regs["sub_bm_tbl"][mxc]), SUBARRAY_BEAM_ENTRIES_PER_MXC
            ):
                hexvals.extend([0, 0, 0, 0])
    return hexvals


def get_total_channels(xpanded_regs) -> int:
    """value for total_channels register"""
    return xpanded_regs["tot_ch"]


def get_num_subarray_beams(xpanded_regs) -> list:
    """
    values for bufN_subarray_beams_table[0|1]
    ie then number of subarray_beam table entries that
    each correlator is to process
    """
    hexvals = []
    for mxc in range(0, NUM_MXC):
        hexvals.append(len(xpanded_regs["sub_bm_tbl"][mxc]))
    return hexvals


def get_packetiser_hex(xpanded_regs) -> list:
    """Get packetiser hex programming values"""
    # TODO
    return []


def expand_compressed_regs(
    compressd_regs, active_subarrays: list, logger
) -> dict | None:
    """
    Expand summary register config into full tables of data to be written
    to register on next scan

    :param register_cfg: list of dictionaries, one per entry in the
    'subarray_beams' register in firmware. List conveys a condensed
    summary of register settings
    :param active_subarrays: list of subarray_ids that should be actively
    producing output data (ie subarray is scanning)

    strategy: assign to matrix correlator 0 until HBM full, then use Mx1
    """

    regs = {
        "sub_bm_tbl": [[] for _ in range(0, NUM_MXC)],
        "vc_dmap": [],
        "vct": [],
        "tot_ch": 0,
        "spead_info": [[] for _ in range(0, NUM_MXC)],
    }

    if compressd_regs is None:
        return regs

    nxt_virt_chan = 0  # next free VCT slot
    nxt_hbm_base = [0 for _ in range(0, NUM_MXC)]  # next HBM address in CT2
    nxt_sbt_entry = [0 for _ in range(0, NUM_MXC)]  # next SubarrayBeam entry
    for sa_entry in compressd_regs:
        subarray = sa_entry["sa_id"]
        if subarray not in active_subarrays:
            continue
        logger.info(f"include reg settings for subarray_{subarray}")
        stns = sa_entry["stns"]
        baselines = (len(stns) * (len(stns) + 1)) / 2
        for tbl_entry in sa_entry["sa_bm"]:
            (
                stn_bm,
                first_coarse,
                first_fine,
                num_fine,
                i_time,
                i_chans,
                my_mxc,  # new - select which MxC used
            ) = tbl_entry

            num_coarse = math.ceil(
                (first_fine + num_fine - 1) / FINE_PER_COARSE
            )

            # save info about each correlation needed for spead-start/end pkts
            regs["spead_info"][my_mxc].append(
                (
                    subarray,
                    stn_bm,
                    first_coarse,
                    first_fine,
                    num_fine,
                    i_time,
                    i_chans,
                    baselines,
                )
            )

            hbm_len = num_fine * CT2_BYTES_PER_STN_FINE_CHAN
            subarray_beam_index = (
                my_mxc * SUBARRAY_BEAM_ENTRIES_PER_MXC + nxt_sbt_entry[my_mxc]
            )

            sbe = (
                nxt_hbm_base[my_mxc],
                first_coarse,
                first_fine,
                num_fine,
                len(stns),
                i_time,
                i_chans,
                num_coarse,
                subarray,
                stn_bm,
            )
            regs["sub_bm_tbl"][my_mxc].append(sbe)
            nxt_hbm_base[my_mxc] += hbm_len
            nxt_sbt_entry[my_mxc] += 1

            for frq_id in range(first_coarse, first_coarse + num_coarse):
                # Assumption: Desired station ordering for visibilities
                # is same order as stations were given to us
                vc_blk_order_ct2 = 0  # order for 4-stn blocks in CT2
                for stn, substn in stns:
                    # one VCT entry for each subarray-beam-freq-stn
                    regs["vct"].append(
                        (
                            subarray,
                            frq_id,
                            stn_bm,
                            stn,
                            substn,
                            nxt_virt_chan,
                        )
                    )
                    # one VC_DEMAP entry per block of 4 stations
                    if nxt_virt_chan % 4 == 0:
                        regs["vc_dmap"].append(
                            (frq_id, vc_blk_order_ct2, subarray_beam_index)
                        )
                        # Expected +=1 Model generates +=4, check with David
                        vc_blk_order_ct2 += 4  # check with David
                    nxt_virt_chan += 1
                # vchans begin on multiple of 4 for each frequency
                nxt_virt_chan = 4 * math.ceil(nxt_virt_chan / 4)
    regs["tot_ch"] = nxt_virt_chan
    # ensure VCT in sorted order so FPGA firmware binary search can work
    regs["vct"] = sorted(regs["vct"], key=_vct_sort_ordering)

    # debug prints ======================================
    logger.info(f"regs= {regs}")

    return regs
