# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
Interface to Correlator FPGA Image

Defines the "peripherals" in the FPGA that the control system will monitor
Defines which aspects of the peripherals to ignore (nodata, noattributes)
"""
import json
import time

import numpy as np
from ska_low_cbf_fpga import FpgaPersonality
from ska_low_cbf_fpga.args_fpga import WORD_SIZE
from ska_low_cbf_model import correlator_model

from ska_low_cbf_proc.icl.correlator.constants import (
    FINE_CH_PER_STANDARD_VIS_CH,
    SEC_PER_FILTERBANK_SAMPL,
)
from ska_low_cbf_proc.icl.correlator.corr_expander import (
    get_num_subarray_beams,
    get_total_channels,
    get_words_demap,
    get_words_subarray_beams,
    get_words_vct,
)
from ska_low_cbf_proc.icl.correlator.spead_sdp import SpeadSdp
from ska_low_cbf_proc.icl.no_attributes import NoAttributes
from ska_low_cbf_proc.icl.no_data import NoData

NUM_BUFFERS = 2  # double-buffered
# These register lengths are for one of the two buffers
VCT_LEN_BYTES = 2048 * WORD_SIZE  # 1024 entries * 2word/entry
DEMAP_LEN_BYTES = 512 * WORD_SIZE  # 256 entries * 2word/entry
SUB_BM_LEN_BYTES = 1024 * WORD_SIZE  # 256 entries * 4w/entry
SUB_BM_TBL_BYTES = 1 * WORD_SIZE  # 1 entries 1w/entry
DEL_TBL_LEN_BYTES = 4096 * WORD_SIZE  # 1k entry * 4 w/entry
CT1_START_PKT_BYTES = 1 * WORD_SIZE  # 1 entry * 1word
# The "total_channels" register should be double buffered in FW
# but isn't currently, so we length to zero (not 1) and then
# both buffers refer to the same register. TODO fix firmware
TOT_CH_LEN_BYTES = 0 * WORD_SIZE  # 1 entry

PKTS_IN_CT1_BUF = 408  # 408 -> 902.48msec of data
SUBARRAY_BEAM_TBL_LEN_PER_MXC = 128

N_VCHAN = 1024
INGEST_STATS_WORDS_PER_VCHAN = 8
INGEST_STATS_PKT_CNT_OFFSET = 1


class CorrFpga(FpgaPersonality):
    """
    Class providing access to Correlator FPGA by translating
    high-level operations (eg VCT update) into lower-level register
    reads and writes

    The implementation of the class methods depends intimately on the
    register names in the FPGA VHDL, plus the function and interaction of
    the registers in the VHDL design. If the VHDL design changes, this code
    may also need to change
    """

    _peripheral_class = {
        "drp": NoAttributes,
        # "system": NoAttributes, # fixme implement?
        "vitis_shared": NoData,
        "cmac_b": NoAttributes,
        "config": NoAttributes,  # fixme implement
        "corr_ct1": NoAttributes,  # fixme implement
        "corr_ct2": NoAttributes,  # fixme implement
        "lfaadecode100g": NoData,
        "spead_sdp": SpeadSdp,
        "spead_sdp_2": SpeadSdp,
        "timeslave": NoAttributes,  # fixme implement
    }

    _not_user_methods = set()
    _not_user_attributes = set()

    VCT_BASE = 8192
    """VCT offset within LFAA data RAM"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # We use new SPEAD init config registers, added in 0.0.2
        self._check_fw("CORR", ">=0.0.2")

        # Save addresses we'll access when updating FPGA register values
        # Note: Use lower-case register names regardless of VHDL case
        periph = self["system"]
        self._uptime = periph["time_uptime"].address
        self._build = periph["build_date"].address

        periph = self["lfaadecode100g"]
        # self._vct_reset = periph["control"].address  # 1w rising edge GOES AWAY
        self._lfaa_reset = periph["lfaa_decode_reset"].address  # 1 word
        self._vct = None
        """Virtual Channel Table base address. 2 words * 1024 entries * 2 buffers"""
        try:
            self._check_fw("CORR", ">=0.0.5")
            self._vct = periph["data"].address + self.VCT_BASE * WORD_SIZE
        except RuntimeError:
            # backward compatibility for older FW versions
            self._vct = periph["vctable"].address
        self._vct_table_sel = periph["table_select"].address  # 1 word
        self._vct_tot_ch = periph["total_channels"].address  # 1 word
        self._vct_counts = periph["spead_packet_count"].address  # 5 words
        self._vcstats = periph["data"].address  # 8 words * 1024 vchans
        self._hbm_reset = periph["hbm_reset"].address
        self._hbm_status = periph["hbm_reset_status"].address

        periph = self["corr_ct1"]

        # self._ct1_del_tabl_0 = periph["del_tab_0"].address
        # self._ct1_del_tabl_1 = periph["del_tab_1"].address

        # self._ct1_reset = periph["full_reset"].address  # REMOVED 2023-08-31
        # self._ct1_start_pkt = periph["scanstartpacketcount"].address  # Gone!
        # self._ct1_frame_start = periph[
        #     "frame_count_buffer0"
        # ].address  # Gone
        # self._ct1_del_tbl_sel = periph["table_select"].address  # Gone
        # self._ct1_del_tbl_active = periph["active_table"].address  # Gone
        # self._ct1_del_tbl_start_pkt = periph[
        #     "table0_startpacket"
        # ].address  # 2w
        # self._ct1_del_tbl = periph["table_0"].address  # 4words * 1k *2buffer
        # self._poly_valid = [
        #     periph["table0_valid"].address,
        #     periph["table1_valid"].address,
        # ]
        self._ct1_stats = periph["early_or_late_count"].address  # 6 words
        self._ct1_vc_pkts_out = periph["correlator_output_count"].address

        periph = self["corr_ct2"]
        self._ct2_tbl_sel = periph["table_select"].address  #
        # n_sub_bms has one entry for each MxC and is double buffered
        self._ct2_n_sub_bms_ctx0 = periph["buf0_subarray_beams_table0"].address
        self._ct2_n_sub_bms_ctx1 = periph["buf1_subarray_beams_table0"].address
        self._ct2_stats = periph["bufferoverflowerror"].address  # 12 Words
        self._ct2_vc_dmap = periph["vc_demap"].address  # 2w * 256 *2buf
        self._ct2_sub_bm = periph["subarray_beam"].address  # 4w*128*2MxC*2buf

        self._buffer_in_use = 1  # first buffer used will be 0, like model
        self.tot_ch = 0  # latest value in total_channels register

        # Although subarray beam table alway has entries for two
        # matrix correlators (0-127, 128-256), sometimes the second packetiser
        # and second matrix correlator won't be present in the FPGA
        # because the FPGA designer has removed it for a faster build
        self._spead_packetiser = [self.spead_sdp]
        if "spead_sdp_2" in self.peripherals:
            self._spead_packetiser.append(self.spead_sdp_2)
        else:
            self._spead_packetiser.append(None)  # no second packetiser

        # hold FPGA in reset while not in use
        self._hbm_reset_and_wait_for_done()

    def corr_scan(
        self,
        exp_regs,
        scanning_subs,
        starting_subs,
        ending_subs,
        cur_subarray_info,
        intrnl_subarray,
    ) -> list:
        """Load correlator registers and start processing"""

        start_time = time.time()

        new_spead_info = []

        # Send END packets before touching existing configuration
        self._send_spead_end(ending_subs, cur_subarray_info)

        if len(scanning_subs) == 0:
            self.stop_corr()
            self._logger.info("WROTE REGISTERS: STOP CORRELATOR")
            return new_spead_info

        # write into the free ping-pong buffer
        buf = self._buffer_in_use + 1
        if buf >= NUM_BUFFERS:
            buf = 0

        # program packetiser with new info ...
        # 1. Enable packetisers so they can send packets (incl start)
        if len(scanning_subs) == 1:
            for pktzr in self._spead_packetiser:
                if pktzr is not None:
                    pktzr.enable()
        # 2. Update each packetiser with data for each entry in subarray_beams
        new_spead_info = exp_regs["spead_info"]
        for mxc_num, pktzr in enumerate(self._spead_packetiser):
            if pktzr is not None:
                pktzr.set_destination_table(
                    new_spead_info[mxc_num], intrnl_subarray
                )
        # Send START packets (both packetisers)
        self._send_spead_start(starting_subs, new_spead_info, intrnl_subarray)

        # Write VCT, VCT demap, Subarray_beam tables, and related registers
        words = np.asarray(get_words_vct(exp_regs), dtype=np.uint32)
        self.driver.write(self._vct + buf * VCT_LEN_BYTES, words)

        words = np.asarray(get_words_demap(exp_regs), dtype=np.uint32)
        self.driver.write(self._ct2_vc_dmap + buf * DEMAP_LEN_BYTES, words)

        words = np.asarray(get_words_subarray_beams(exp_regs), dtype=np.uint32)
        self.driver.write(self._ct2_sub_bm + buf * SUB_BM_LEN_BYTES, words)

        tc_word = get_total_channels(exp_regs)
        self.driver.write(self._vct_tot_ch + buf * TOT_CH_LEN_BYTES, tc_word)

        words = np.asarray(get_num_subarray_beams(exp_regs), dtype=np.uint32)
        ct0_nbm = words[0]
        ct1_nbm = words[1]
        adr_offst = buf * SUB_BM_TBL_BYTES
        self.driver.write(self._ct2_n_sub_bms_ctx0 + adr_offst, ct0_nbm)
        self.driver.write(self._ct2_n_sub_bms_ctx1 + adr_offst, ct1_nbm)

        # Flip VCT, VCT_Demap, Subarray_beam  buffers
        self._driver.write(self._vct_table_sel, buf)
        # self._driver.write(self._ct1_del_tbl_sel, buf) # To be replaced
        self._driver.write(self._ct2_tbl_sel, buf)
        self._buffer_in_use = buf
        self.tot_ch = tc_word

        if len(scanning_subs) == 1:
            # To be replaced with new DELAY POLY Buffers

            # (re-)assert reset as a precaution (should be already asserted)
            self._hbm_reset_and_wait_for_done()
            # release reset, starting "latch-on" process (may take 845msec)
            self._hbm_and_ingest_run()

        run_time_ms = (time.time() - start_time) * 1000.0
        self._logger.info("WROTE CORRELATOR REGISTERS: %.1f msec", run_time_ms)

        return new_spead_info

    def stop_corr(self):
        """Stop correlator producing data"""
        self._hbm_reset_and_wait_for_done()

    def _hbm_reset_and_wait_for_done(
        self, sleep_secs=0.005, timeout_count=100
    ):
        """
        Reset to stop HBM/Ingest allowing VCT modifications to be made

        Note: The current FPGA requires all subarrays to stop if VCT
        is to be changed, hence it can only properly support a single subarray
        Consequently this software can only work with a single subarray config
        """
        self.driver.write(self._hbm_reset, 0x1)
        cnt = 0
        while (self.driver.read(self._hbm_status) != 0x1) and (
            cnt < timeout_count
        ):
            time.sleep(sleep_secs)
            cnt += 1
        if cnt >= timeout_count:
            self._logger.error("HBM reset did not complete")
            return
        self.driver.write(self._lfaa_reset, 0x1)

    def _hbm_and_ingest_run(self):
        """
        Restart ingest/CT1 after new VCT parameters were written

        Note comment on _hbm_reset_and_wait_for_done()
        """
        self.driver.write(self._hbm_reset, 0x0)
        self.driver.write(self._lfaa_reset, 0x0)

    def _send_spead_end(self, ending_sa, spead_info):
        n_end_pkts = 0
        for mxc_num, mxc_regs in enumerate(spead_info):
            for sb_index, entry in enumerate(mxc_regs):
                sa_id, _, _, _, num_fine, _, i_chans, _ = entry

                # only send if this subarray is stopping
                if sa_id not in ending_sa:
                    continue
                if self._spead_packetiser[mxc_num] is not None:
                    self._spead_packetiser[mxc_num].send_end_packets(sb_index)
                num_integrated_freq_chans = num_fine // i_chans
                n_end_pkts += num_integrated_freq_chans
        if n_end_pkts != 0:
            self._logger.info("SPEAD END - %d end packets)", n_end_pkts)

    def _send_spead_start(self, starting_sa, spead_info, intrnl_subarray):
        """
        Send spead start packets for subarrays that are starting
        Start packets must be sent from the correct packetiser (the one that is
        handling the correlation data output)
        """
        nxt_sa_bm_vis_ch = {}  # subarray-beam visibility channel numbers
        n_start_pkts = 0  # counter of spead-start packets we send
        for mxc_num, mxc_regs in enumerate(spead_info):
            for sb_index, reg_val in enumerate(mxc_regs):
                (
                    sa_id,
                    stn_bm,
                    first_coarse,
                    first_fine,
                    num_fine,
                    i_samples,
                    i_chans,
                    baselines,
                ) = reg_val

                # only send if this subarray is starting
                if sa_id not in starting_sa:
                    continue

                # first vis chan of a subarray beam is always zero
                sa_bm_name = f"{sa_id}.{stn_bm}"
                if sa_bm_name not in nxt_sa_bm_vis_ch:
                    nxt_sa_bm_vis_ch[sa_bm_name] = 0

                num_integrated_freq_chans = num_fine // i_chans
                scanid = intrnl_subarray[str(sa_id)]["scan_id"]  # TODO

                cfg = {
                    # FIXME - workarounds to trick model into giving expected answer
                    "fine_start": 0,  # first_fine,
                    "n_fine_integrate": 24,  # num_integrated_freq_chans,
                    # TODO: should vis chan number be based on chan frequency?
                    # (currently counter relative to start of visibility)
                    "frequency_channel": nxt_sa_bm_vis_ch[sa_bm_name],
                    "nb_baseline": baselines,
                    "scan_id": scanid,
                    "subarray": sa_id,  # TODO duplicate see below?
                    "beam": stn_bm,
                    "coarse_start": first_coarse,
                    "subarray_id": sa_id,  # TODO duplicate see above?
                    "integration_period": i_samples * SEC_PER_FILTERBANK_SAMPL,
                    "freq_bandwidth": 390625 / 72,  # Hertz exact
                    "resolution": 32,  # 32-bit float
                    "zoom_id": 0,  # Not a zoom
                }
                nxt_sa_bm_vis_ch[sa_bm_name] += num_integrated_freq_chans

                pktizr = self._spead_packetiser[mxc_num]
                pktzr_entry = sb_index
                if pktizr is not None:
                    # Note: assumes hardware sends faster than we make requests
                    self.send_initial_spead_packets(
                        json.dumps(cfg), pktzr_entry, pktizr
                    )
                    n_start_pkts += num_integrated_freq_chans
        if n_start_pkts != 0:
            self._logger.info("SPEAD INIT - %d start packets", n_start_pkts)

    def read_packet_counts(self):
        """
        Read most recent packet count received for each VirtualChannel

        :return: list of counters, one per virtual chan
        """
        n_words = N_VCHAN * INGEST_STATS_WORDS_PER_VCHAN
        pkt_cnts = self._driver.read(self._vcstats, n_words)
        return pkt_cnts[
            INGEST_STATS_PKT_CNT_OFFSET:n_words:INGEST_STATS_WORDS_PER_VCHAN
        ].tolist()

    def zero_packet_counts(self):
        """Clear packet counters in FPGA registers."""
        n_words = N_VCHAN * INGEST_STATS_WORDS_PER_VCHAN
        np_data = np.zeros(n_words, dtype=np.uint32)
        self.driver.write(self._vcstats, np_data)

    @property
    def _packed_fw_version(self) -> int:
        """
        FPGA Firmware version packed into 4 bytes.

        major.minor.patch.prerelease (major being the high byte)

        For releases, the prerelease field will be zero.
        For non-release builds, the prerelease field will contain the lower case form of
        the first non-whitespace character of the build type (e.g. "d" for "dev").

        e.g. For Firmware version 1.2.3+dev<commit>, we get 0x01020364
        """
        prerelease = 0
        if hasattr(self.system, "build_type"):
            # build_type register was added to firmware in Sep 2023
            build_type = (
                int.to_bytes(self.system.build_type.value, WORD_SIZE, "big")
                .decode(encoding="ascii")
                .lower()
                .strip()
            )
            if build_type != "rel":
                # ASCII marker for prerelease, first character of build type
                prerelease = ord(build_type[0])
                if prerelease == 0:
                    # in case we get zero in error, use '!'
                    prerelease = 0x21
        return (
            (((self.system.firmware_major_version) & 0xFF) << 24)
            | (((self.system.firmware_minor_version) & 0xFF) << 16)
            | (((self.system.firmware_patch_version) & 0xFF) << 8)
            | (prerelease & 0xFF)
        )

    @property
    def _hardware_id(self) -> int:
        """
        The hardware ID is 32-bits and needs to be unique across all the
        hardware that produces visibilities.
        """
        # TODO use of FPGA MAC consistently crashes on 3/5/2023. Why?
        # use the low 4 bytes of the FPGA's first MAC address
        if self.info:
            return int(
                "".join(
                    self.info["platform"]["macs"][0]["address"].split(":")[2:]
                ),
                16,
            )
        return 0xABCD0123

    # TODO - the below function only really existed to test the functionality
    #  intitally. We can refactor it away if/when we stop using the model here.
    def send_initial_spead_packets(
        self, configuration_string: str, sb_index: int, pktizr: SpeadSdp
    ):
        """
        Configure the SPEAD initialisation template & send packets.

        :param configuration_string: configuration of the SPEAD stream
        e.g
        '''{"frequency_channel": 1, "nb_baseline": 21, "scan_id": 1,
         "subarray": 1, "beam": 21, "coarse_start": 65,
         "fine_start": 0, "n_fine_channels": 144,
         "subarray_id": 1, "integration_period": 0.849,
         "freq_bandwidth": 5425.3, "resolution": 1, "zoom_id": 1}'''
        :param sb_index: int, index in subarray_beam table of the entry
         that is being initialised
        :param pktizr: SPEAD to SDP packetiser peripheral instance
        """
        configuration = json.loads(configuration_string)
        self._logger.warning(f"SPEAD INIT config: {configuration_string}")
        template = correlator_model.generate_initialisation_template(
            frequency_channel=int(configuration["frequency_channel"]),
            nb_baseline=int(configuration["nb_baseline"]),
            scan_id=int(configuration["scan_id"]),
            subarray=int(configuration["subarray"]),
            beam=int(configuration["beam"]),
            coarse_start=int(configuration["coarse_start"]),
            fine_start=int(configuration["fine_start"]),
            n_fine_integrate=int(configuration["n_fine_integrate"]),
            hardware_id=self._hardware_id,
            subarray_id=int(configuration["subarray_id"]),
            integration_period=float(configuration["integration_period"]),
            freq_bandwidth=float(configuration["freq_bandwidth"]),
            resolution=int(configuration["resolution"]),
            zoom_id=int(configuration["zoom_id"]),
            firmware_id=self._packed_fw_version,
        )
        pktizr.configure_init_packets(
            template,
            configuration["coarse_start"],
            FINE_CH_PER_STANDARD_VIS_CH,
        )
        # FIXME recover locally without using exceptions in low-level code
        try:
            pktizr.send_initial_packets(sb_index)
        except RuntimeError as err:
            self._logger.error(err)

    def fpga_status(self) -> dict:
        """
        Gather status indication for FPGA processing operations
        """
        summary = {}
        summary["fpga_uptime"] = self["system"]["time_uptime"].value
        summary["total_pkts_in"] = self["system"][
            "eth100g_rx_total_packets"
        ].value
        summary["spead_pkts_in"] = self["lfaadecode100g"][
            "spead_packet_count"
        ].value
        summary["total_pkts_out"] = self["system"][
            "eth100g_tx_total_packets"
        ].value
        summary["vis_pkts_out"] = (
            self["spead_sdp"]["no_of_packets_sent"].value
            + self["spead_sdp_2"]["no_of_packets_sent"].value
        )
        return summary

    def update_delay_polys(self, buffer_pair: list) -> None:
        """
        Write delay polynomial data into firmware registers

        :param buffer_pair: Two-element list, each element a 20kword list
        of integers to be written to FPGA delay polynomial buffers as uint32
        """
        # The delay registers may not yet exist in firmware
        try:
            # Have to convert to numpy arrays for FPGA driver (FIXME?)
            self["corr_ct1"]["data"][0:20480] = np.asarray(
                buffer_pair[0], dtype=np.uint32
            )
            self["corr_ct1"]["data"][20480:40960] = np.asarray(
                buffer_pair[1], dtype=np.uint32
            )
        except (KeyError, ValueError):
            self._logger.warning("Can't write delays into register")
        return
