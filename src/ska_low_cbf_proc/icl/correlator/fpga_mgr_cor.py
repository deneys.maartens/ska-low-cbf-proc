# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""FPGA Manager class for Correlator Personality"""

import struct

from ska_low_cbf_proc.icl.correlator.constants import SKA_COARSE_SPACING_GHZ
from ska_low_cbf_proc.icl.correlator.corr_expander import (
    expand_compressed_regs,
)
from ska_low_cbf_proc.icl.fpga_mgr import FpgaMgr
from ska_low_cbf_proc.processor.delay_subscription import DelayPolySubscriber

POLY_IS_VALID_BIT = 0x1_0000_0000


def get_subarray_lists(subarray_info):
    """
    From global subarray information (published by allocator), extract
    - sorted list of all subarray_ids
    - sorted list of scanning subarray_ids
    """
    all_subarray_ids = set()
    scan_subs = set()
    for (
        subarray_id_txt,
        subarray_description,
    ) in subarray_info.items():
        subarray_id = int(subarray_id_txt)
        all_subarray_ids.add(subarray_id)

        if subarray_description["scanning"]:
            scan_subs.add(subarray_id)
    return sorted(list(all_subarray_ids)), sorted(list(scan_subs))


def get_delay_uris(subarray_info, logger) -> list[tuple]:
    """
    From global subarray info (published by allocator), extract a list
    of tuples describing delay polynomial subscription URIs

    :return: list of (subarray_id, beam_id, tango_device, attribute) tuples
    """
    dly_uri_info = []
    for (
        subarray_id_txt,
        subarray_description,
    ) in subarray_info.items():
        subarray_id = int(subarray_id_txt)
        # get delay URI for each beam a subarray has
        for beam in subarray_description["stn_beams"]:
            beam_id = int(beam["beam_id"])
            delay_poly_src = beam["delay_poly"]
            try:
                dev, attr = delay_poly_src.rsplit("/", 1)
            except ValueError:
                logger.error(
                    "Not a valid Tango delay polynomial uri: %s",
                    delay_poly_src,
                )
                continue
            dly_uri_info.append((subarray_id, beam_id, dev, attr))
    return dly_uri_info


class SubscriptionMonitor:
    """A simple convenience class to keep track of failed subscriptions"""

    def __init__(self):
        self._failed_delays = {}
        """Keeps track of failed delay polynomial subscriptions; format:
        { #subarray   beams
            1:        [1, 2, 3],
            3:        [7, 8],
            ...
        }
        """

    def add(self, subarray_id: int, beam_id: int) -> bool:
        """Make note of the specified subarray-beam.
        Return True if our records are modified; False otherwise.
        """
        if subarray_id not in self._failed_delays:
            self._failed_delays[subarray_id] = []
        if beam_id not in self._failed_delays[subarray_id]:
            self._failed_delays[subarray_id].append(beam_id)
            return True
        return False

    def remove(self, subarray_id: int, beam_id: int) -> bool:
        """Remove the specified subarray-beam.
        Return True if our records are modified; False otherwise.
        """
        if (
            subarray_id in self._failed_delays
            and beam_id in self._failed_delays[subarray_id]
        ):
            self._failed_delays[subarray_id].remove(beam_id)
            return True
        return False

    def is_ok(self, subarray_id: int, beam_id: int) -> bool:
        "Has the subscription for the specified subarray-beam failed?"
        return not (
            subarray_id in self._failed_delays
            and beam_id in self._failed_delays[subarray_id]
        )

    def get(self) -> dict:
        "Get the current state (dictionary)."
        return self._failed_delays


class FpgaMgrCor(FpgaMgr):
    """
    A class for Correlator FPGAs that monitors and programs FPGA registers

    This class translates parameters specified by the Allocator into register
    updates. It also periodically monitors FPGA registers that indicate the
    quality of the ongoing operation of the PST FPGA
    """

    PERIODIC_PRINT_SECS = 30.0
    MAX_DELAY_POLY_QUEUE = 12
    SECS_PER_PKT = 2211840e-9  # packets have 2048 samples @ 1080ns/sample
    PKTS_PER_INTEGRATION = 384
    FPS_NUMER = 390625  # numerator of correlator filterbank frames per sec
    FPS_DENOM = 331776  # denominator of correlator filterbank frames per sec

    def __init__(
        self,
        fpga_instance,
        delay_stats_cbk,
        io_stats_cbk,
        subscr_stats_cbk,
        logger,
    ):
        """
        :param delay_stats_cbk: A function expecting a list argument.
        (See processor_device:update_delay_stats)

        The list will be published via Tango attribute to provide
        information about subarray processing in the FPGA.
        One list entry per subarray-beam.
        """
        super().__init__(fpga_instance, logger)
        # override periodic timer interval for this specific FPGA personality
        self._periodic_secs = 4.0  # i.e. faster than 10-sec delay update

        self.logger.info("Initialising CORRELATOR FPGA interactions")
        self.fpga = fpga_instance

        self._subs_configured = []  # list of configured subarray_ids
        self._subs_scanning = []  # list of scanning subarray_ids
        self._spead_start_info = []

        self._seconds = FpgaMgrCor.PERIODIC_PRINT_SECS
        self._subarray_delay_subs = {}  # subarray-beam delay-subscriptons
        self._poly_q = {}  # queue of (epoch, poly) from subscriptions
        self._polys_avail = {}  # key=subarray.beam, pair of polys for use
        self._was_any_poly_updated = False
        self._prev_vchan_pkt_nos = None  # pkt numbers for 1024 vchans

        self._exp_regs = None  # info about all register sets

        # Initially publish empty statistics list since no subarrays exist
        self._stats_cbk = delay_stats_cbk
        delay_stats_cbk([])
        self._highest_pkt_no = 0
        self._highest_pkt_secs = 0
        self._highest_blk_no = 0  # 0.849 second integration blocks
        self._pkt_spread = 0  # diff btw earliest & latest received pkt nos
        self._io_stats_cbk = io_stats_cbk
        self._subscr_stats_cbk = subscr_stats_cbk
        self._subscr_monitor = SubscriptionMonitor()

    def do_register_changes(self, cmd):
        """
        Called to change FPGA register settings arising from changes to
        allocator attributes that specify the FPGA state
        :param cmd: Dictionary of parameter change commands
        """
        if "subarray" in cmd:
            subarray_descr = cmd["subarray"]
            cpr_regs = cmd["regs"]
            self._program_regs(subarray_descr, cpr_regs)
        else:
            self.logger.warn("Unrecognised register command")

    def periodic_actions(self):
        """
        Called to periodically check or update FPGA registers
        (period in self._periodic_secs)
        """

        # Print liveness message
        self._seconds += self._periodic_secs
        if self._seconds >= FpgaMgrCor.PERIODIC_PRINT_SECS:
            self._seconds -= FpgaMgrCor.PERIODIC_PRINT_SECS
            self.logger.info("Correlator periodic checks")
            fpga_io_status = self.fpga.fpga_status()
            self.logger.info("fpga_status: %s", str(fpga_io_status))
            self._io_stats_cbk(fpga_io_status)

        # Update delay polynomials if there are scanning subarrays
        if len(self._subs_scanning) != 0:
            # These calls modify class variables, need to be done in order...
            self._check_current_pkt_counters()  # read SPS packet time
            self._queue_subscribed_polys()  # get delays from subscriptions
            self._prep_polys_for_use()  # pick the two polys to use
            self._update_delay_registers()  # write delay polys to FPGA
            self._update_subscription_valid()
            self._update_delay_stats()  # publish stats about delays

    def _check_current_pkt_counters(self):
        """
        Read current ingest packet counters:
            - check SPS data is being received on all in-use virtual channels
            - find highest packet number
            - for debug, log the spread of the incoming packet numbers

        :return: highest packet number seen so far on any virtual channel
        """
        # Which virtual channel numbers are currently in use?
        vchans_active = [vc_info[5] for vc_info in self._exp_regs["vct"]]
        num_vchans = len(vchans_active)
        if num_vchans == 0:
            return 0

        all_pkt_ctrs = self.fpga.read_packet_counts()
        # only consider virtual channels that are in use
        vchan_pkt_cntrs = [
            all_pkt_ctrs[i]
            for i in range(0, len(all_pkt_ctrs))
            if i in vchans_active
        ]

        highest_pkt = max(vchan_pkt_cntrs)
        pkt_spread = 0
        if (self._prev_vchan_pkt_nos is not None) and (
            len(self._prev_vchan_pkt_nos) == len(vchan_pkt_cntrs)
        ):
            changes = [
                vchan_pkt_cntrs[i] - self._prev_vchan_pkt_nos[i]
                for i in range(0, num_vchans)
            ]
            max_delta = max(changes)
            min_delta = min(changes)
            if max_delta == 0:
                self.logger.warning(
                    "No new SPS packets (pkt_no=%s)",
                    highest_pkt,
                )
            elif min_delta == 0:
                self.logger.warning(
                    "pkt_no=%s, some missing packets",
                    highest_pkt,
                )
            else:
                # find lowest packet counter that's actually incrementing
                lowest = highest_pkt
                for cnt in range(0, num_vchans):
                    # is this virtual channel getting any packets?
                    if min_delta > 0:
                        lowest = min(lowest, vchan_pkt_cntrs[cnt])
                pkt_spread = highest_pkt - lowest
                # log recent SPS packet number info
                self.logger.debug(
                    "pkt_no=%s (spread %s)",
                    highest_pkt,
                    pkt_spread,
                )
        # save current pkt nos for comparison at next read
        self._prev_vchan_pkt_nos = vchan_pkt_cntrs

        # save current SPS time as highest packet count
        self._highest_pkt_no = highest_pkt
        # save SPS time as seconds since J2000 epoch
        self._highest_pkt_secs = highest_pkt * self.SECS_PER_PKT
        # save SPS time as integration_block after J2000 epoch
        self._highest_blk_no = highest_pkt / self.PKTS_PER_INTEGRATION
        # save earliest-to-latest spread in packet numbers
        self._pkt_spread = pkt_spread

    def _update_subscription_valid(self) -> None:
        """Update local record keeping track whether delay poly. subscriptions
        have failed.
        """
        modified = False
        for name, subscriber in self._subarray_delay_subs.items():
            # decompose into subarray / beam
            subarray, beam = [int(_) for _ in name.split(".")]
            if subscriber.failed:
                modified |= self._subscr_monitor.add(subarray, beam)
                continue
            # otherwise purge this subarray-beam if it failed previously
            modified |= self._subscr_monitor.remove(subarray, beam)

        if modified:
            self._subscr_stats_cbk(self._subscr_monitor.get())

    def _queue_subscribed_polys(self) -> None:
        """
        Drain polynomials from subscriptions (self._subarray_delay_subs)
        and place in local queue (self._poly_q).
        Groom queue to contain only relevant polynomials:
        - not too old,
        - not too many,
        - no duplicates
        """

        for name, dly_obj in self._subarray_delay_subs.items():
            # newly added subarray-beams need storage
            if name not in self._poly_q:
                self._poly_q[name] = []
                self._polys_avail[name] = [None, None]

            dly_obj.queue_subscribed_polys(
                self._highest_blk_no, self.FPS_NUMER, self.FPS_DENOM, name
            )

    def _prep_polys_for_use(self) -> None:
        """
        Choose the two delay polynomials (from self._poly_q) that should be
        currenly used for each beam and update/save in self._polys_avail
        This is done for all configured subarray_beams, even if not scanning
        so that polynomials are ready whenever scan may be commanded
        """

        for beam_name, dly_obj in self._subarray_delay_subs.items():
            was_poly_updated = dly_obj.prep_polys_for_use(
                beam_name, self._highest_blk_no
            )

            self._was_any_poly_updated = (
                was_poly_updated or self._was_any_poly_updated
            )

    def _update_delay_registers(self) -> None:
        """
        Write the two prepared polynomials in self._polys_avail to registers.

        self._polys_avail contains two latest polynomials for each beam. First
        one of the two should be writen to the first hardware buffer, second
        one should be written to the second buffer.

        Both buffers will be completely written, but in some places the data
        written will be the same as the data already in the buffer

        Buffers holding data to be programmed to FPGA are initialised with
        floating-pt zero == integer zero (all zero bits)
        One 20-word entry per Virtual channel * 1024 virt chans = 20k words
        """
        if not self._was_any_poly_updated:
            return

        words_buffer = [
            [0] * 20 * 1024,
            [0] * 20 * 1024,
        ]

        for sa_id, frq_id, bm_id, stn, sstn, vc in self._exp_regs["vct"]:
            name = f"{sa_id}.{bm_id}"
            missing_names = []
            if (name not in self._subarray_delay_subs) and (
                name not in missing_names
            ):
                self.logger.error(
                    "No delay poly for subarray %s beam %s", sa_id, bm_id
                )
                missing_names.append(name)

            polys_avail = self._subarray_delay_subs[name].get_polys_avail()
            base_word = 20 * vc
            for buffer_num, poly in enumerate(polys_avail):
                if poly is None:
                    continue  # zero present from initialisation of buffer
                # _, blk_num, offset_sec, delay_details, _ = poly
                # find station's delay in poly
                coeffs = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                pol_offset_ns = 0
                delay_details = poly.poly
                for itm in delay_details:
                    if (
                        itm["station_id"] == stn
                        and itm["substation_id"] == sstn
                    ):
                        coeffs = itm["xypol_coeffs_ns"]
                        # pad polynomials that only have the constant term
                        while len(coeffs) < 6:
                            coeffs.append(0.0)
                        pol_offset_ns = itm["ypol_offset_ns"]
                        break
                # Collect data elements needed for FPGA programming
                sky_ghz = frq_id * SKA_COARSE_SPACING_GHZ
                offset_sec = poly.offset_sec
                blk_num = poly.first_valid_blk_no
                blk_num = blk_num | POLY_IS_VALID_BIT
                # convert fp and int values into 32-bit unsigned for registers
                tmp_bytes = struct.pack(
                    "<dddddddddQ",
                    *coeffs,  # no need for negation. firmware does that
                    sky_ghz,
                    offset_sec,
                    pol_offset_ns,
                    blk_num,
                )
                words_buffer[buffer_num][
                    base_word : base_word + 20
                ] = struct.unpack("<IIIIIIIIIIIIIIIIIIII", tmp_bytes)
        self.fpga.update_delay_polys(words_buffer)

    def _update_delay_stats(self):
        """
        Get info about the two delay-polynomials programmed into FPGA registers
        Note: Only scanning subarrays have will have register entries
        - polynomial start times
        - validity (valid if either is in use)
        - Delay nanoseconds (NOT poly) for each station (evaluated at T=0)
        """
        if not self._was_any_poly_updated:
            return
        self._was_any_poly_updated = False

        sb_start_times = []
        sb_idx = {}
        sps_pkt_time = self._highest_pkt_secs
        # for each polynomial currently ready to be programmed into FPGA
        for name, dly_obj in self._subarray_delay_subs.items():
            subarray_id, beam_id = name.split(".")
            subarray_id = int(subarray_id)
            beam_id = int(beam_id)
            # only show polynomial stats for scanning subarrays
            if subarray_id not in self._subs_scanning:
                continue
            # make list of start-of-validity epoch seconds
            polys_avail = dly_obj.get_polys_avail()  # The two polys in FPGA
            epochs_programmed = []
            is_delay_valid = False
            earliest_poly_idx = -1  # ie none found (use none -> linter err)
            for poly_info in polys_avail:
                if poly_info is None:
                    epochs_programmed.append(None)
                else:
                    start_time = poly_info.epoch
                    valid_time = poly_info.valid_secs
                    epochs_programmed.append(start_time)
                    if start_time <= sps_pkt_time <= (start_time + valid_time):
                        is_delay_valid = True
                    # Is this polynomial the earliest epoch (ie likely in-use)
                    if (earliest_poly_idx == -1) or (
                        (earliest_poly_idx != -1)
                        and (start_time < epochs_programmed[earliest_poly_idx])
                    ):
                        earliest_poly_idx = len(epochs_programmed) - 1
            # make list of delay values for each station in subarray
            # corresponding to the start of the earliest delay poly (ie in use)
            stn_delay_ns = []
            if earliest_poly_idx != -1:
                dly_detail = polys_avail[earliest_poly_idx].poly
                for stn_dly_info in dly_detail:
                    stn_dly = {
                        "stn": stn_dly_info["station_id"],
                        # TODO: add substation into stats info?
                        "ns": stn_dly_info["xypol_coeffs_ns"][0],
                    }
                    stn_delay_ns.append(stn_dly)

            # copy start-of-validity times to published structure
            # structure is list-of-dicts, one dict in list per subarray
            if subarray_id not in sb_idx:
                sb_start_times.append(
                    {"subarray_id": subarray_id, "beams": []}
                )
                sb_idx[subarray_id] = len(sb_start_times) - 1
            subscription_ok = self._subscr_monitor.is_ok(subarray_id, beam_id)
            sb_start_times[sb_idx[subarray_id]]["beams"].append(
                {
                    "beam_id": beam_id,
                    "valid_delay": is_delay_valid,
                    "subscription_valid": subscription_ok,
                    # "delay_q_len": len(self._poly_q[name]),
                    "delay_start_secs": epochs_programmed,
                    # Do we really need to publish this bulky info?
                    # ie list can have up to 1024 dict entries
                    "stn_delay_ns": stn_delay_ns,
                }
            )
        # add info about current packet numbers
        # info only correct if at least one subarray is scanning
        if len(self._subs_scanning) != 0:
            extra_info = {
                "current_secs": self._highest_pkt_secs,
                "pkt_no_spread": self._pkt_spread,
            }
            sb_start_times.append(extra_info)

        # update attribute via the callback
        self._stats_cbk(sb_start_times)

    def _program_regs(self, all_subarrays_info, cpr_regs):
        """
        Program FPGA registers if scanning subarrays has changed

        :param all_subarrays_inf: dict of all subarray info from allocator's
                "internal_subarray" atrribute
        :param cpr_regs: compressed register descriptions from allocator's
                "internal_alveo" attribute
        """

        subs_configured, subs_scanning_new = get_subarray_lists(
            all_subarrays_info
        )
        self.logger.info("Subarrays: %s", subs_configured)
        self.logger.info("Subarrays scanning: %s", subs_scanning_new)

        sub_dly_poly_sbda = get_delay_uris(all_subarrays_info, self.logger)
        self._subscribe_delay_polys(sub_dly_poly_sbda)

        # No registers to update if list of scanning subarrays unchanged
        if subs_scanning_new == self._subs_scanning:
            # remove delay-poly subscriptions for any deconfigured subarrays
            self._prune_delay_subscriptions(subs_configured)
            self._prune_prepared_polys(subs_configured)
            return

        self.logger.info(
            "Scanning subarrays change %s -> %s",
            self._subs_scanning,
            subs_scanning_new,
        )

        # Which subarrays are ending scans?
        ending_ids = [
            sa for sa in self._subs_scanning if sa not in subs_scanning_new
        ]
        # which subarrays are starting scans?
        starting_ids = [
            sa for sa in subs_scanning_new if sa not in self._subs_scanning
        ]

        # Uncompress register settings for all scanning subarrays
        self._exp_regs = expand_compressed_regs(
            cpr_regs, subs_scanning_new, self.logger
        )

        # Program FPGA to calculate correlations for scanning subarrays
        new_spead_start_info = self.fpga.corr_scan(
            self._exp_regs,
            subs_scanning_new,
            starting_ids,
            ending_ids,
            self._spead_start_info,
            all_subarrays_info,
        )

        # remove delay subscriptions for subarrays no longer configured
        self._prune_delay_subscriptions(subs_configured)
        self._prune_prepared_polys(subs_configured)

        # clean up published subarray delay-poly info
        if len(subs_scanning_new) == 0:
            self._stats_cbk([])

        # save current working parameters
        self._spead_start_info = new_spead_start_info
        self._subs_configured = subs_configured
        self._subs_scanning = subs_scanning_new

    def _subscribe_delay_polys(self, sub_dly_poly_sbda) -> None:
        """
        Make sure we have Tango event subscriptions for delay polynomials
        of all subarray beams.

        :param sub_dly_poly_sbda: list of all delay polys for all subarrays
        :return: list of subarray-ids corresponding all active subarrays
        """
        for itm in sub_dly_poly_sbda:
            subarray_id, beam_id, dev, attr = itm
            # use "subarray_id.beam_id" as key for subscriptions
            beam_poly_name = f"{subarray_id}.{beam_id}"
            # subscribe to events if not already subscribed
            if beam_poly_name not in self._subarray_delay_subs:
                self.logger.info(
                    "Subscribe Delay subarray_%d, beam_%d dev=%s attr=%s",
                    subarray_id,
                    beam_id,
                    dev,
                    attr,
                )
                self._subarray_delay_subs[
                    beam_poly_name
                ] = DelayPolySubscriber(self.logger)
                self._subarray_delay_subs[beam_poly_name].subscribe(dev, attr)

    def _prune_delay_subscriptions(self, configured_subarray_ids):
        """
        Unsubscribe from delays for any subarray not in the argument list

        :param subarray_ids: ids of all in-use (configured) subarrays
        """
        unused_subarray_beams = []
        subscr_modified = False
        for name, subscriber_obj in self._subarray_delay_subs.items():
            s_id, b_id = name.split(".")
            if int(s_id) in configured_subarray_ids:
                # don't touch delay subscription if subarray is configured
                continue
            # remove delay subscription because subarray no longer configured
            self.logger.info("Unsub Delay subarray_%s beam_%s", s_id, b_id)
            subscriber_obj.unsubscribe()
            unused_subarray_beams.append(name)
            subscr_modified |= self._subscr_monitor.remove(
                int(s_id), int(b_id)
            )
        for name in unused_subarray_beams:
            # free delay-subscriber objects to allow GC
            self._subarray_delay_subs.pop(name)
        if subscr_modified:
            self._subscr_stats_cbk(self._subscr_monitor.get())

    def _prune_prepared_polys(self, configured_subarray_ids):
        """
        When subarray is no longer configured, Remove subarray-beams from:
        - queue of incoming polynomials (self._poly_q)
        - delay polynomials ready for use in dual-buffer (self.polys_avail)

        :param scanning_subarray_ids: list of IDs of scanning subarrays
        """
        beams_to_remove = []
        for name in self._polys_avail:
            s_id, _ = name.split(".")
            s_id = int(s_id)
            if s_id not in configured_subarray_ids:
                beams_to_remove.append(name)
        for name in beams_to_remove:
            # These two always deleted together
            self._poly_q.pop(name)
            self._polys_avail.pop(name)

    def cleanup(self):
        """
        Final Tasks before exit of FPGA monitoring thread
        """

        # stop any scanning subarrays
        scanning_ids = [sa for sa in self._subs_scanning]
        if len(scanning_ids) > 0:
            self.logger.info(
                "Stopping compute for subarrays: %s", str(scanning_ids)
            )
            self.fpga.corr_scan(
                self._exp_regs,  # unused during all-stop
                [],  # no subarrays continue to scan, all-stop
                [],  # unused during all-stop, no spead-init sent
                scanning_ids,  # send spead-end for these
                self._spead_start_info,
                None,  # unused for all-stop
            )

        # unsubscribe all delay-polynomial attribute subscriptions
        configured_subarrays = []  # no subarrays to keep
        self._prune_delay_subscriptions(configured_subarrays)
        self._prune_prepared_polys(configured_subarrays)  # optional, really
        # Clear packet counter registers
        self._prev_vchan_pkt_nos = None  # unnecessary: thread exiting
        self.fpga.zero_packet_counts()  # needed: fpga image may be re-used
