# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.
"""SPEAD output to SDP FPGA Peripheral abstraction layer (ICL)"""
import math
import time
from socket import inet_aton

import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral, IclField
from ska_low_cbf_fpga.args_fpga import WORD_SIZE, ArgsWordType

from ska_low_cbf_proc.icl.correlator.constants import (
    FINE_CHANNEL_HZ,
    SKA_COARSE_SPACING_HZ,
)

# number of subarray-beam table entries handled by a SDP packetiser
PKTIZR_SBT_ENTRIES = 128


def first_visibility_freq(first_freq_id: int, n_fine_ch: int) -> float:
    """
    Calculate the first visibility's centre frequency for a correlation.

    :param first_freq_id: SPS Frequency ID (aka coarse channel)
    :param n_fine_ch: Number of fine channels integrated in our
      correlation.
    :return: Base Frequency (Hz) for SPEAD init packets to SDP
    """
    return (
        first_freq_id * SKA_COARSE_SPACING_HZ  # centre frequency
        # minus half of the total fine channels BW, gives centre of lowest fine ch.
        - (3456 / 2) * FINE_CHANNEL_HZ
        # minus half a fine channel, gives bottom frequency of our visibility
        - FINE_CHANNEL_HZ / 2
        # plus half a Correlation BW, gives centre freq. of first visibility
        + FINE_CHANNEL_HZ * n_fine_ch / 2
    )


def binary_fraction(x) -> (int, int):
    """
    Convert a number to two words as binary fixed point.

    :returns: integer part, fractional part
    """
    int_part = int(x)
    frac_part = int((x - int_part) * 0x1_0000_0000)
    return int_part, frac_part


def float_from_binary_fraction(integer, fraction) -> float:
    """Convert two words of binary fixed point to a float."""
    return ((integer << 32) | fraction) / 0x1_0000_0000


def _get_vis_chans_per_sdp_host(beam_def) -> int:
    """
    From the subarray definition work out the number of visibility channels
    that a SDP server can receive.
    """
    host_list = beam_def["host"]
    # assume first SDP server can receive all ports if only one server
    if len(host_list) == 1:
        return 1_000_000  # more than low.cbf will ever generate
    # otherwise gap between first channel for two servers is the value
    ch0, _, _ = host_list[0]
    ch1, _, _ = host_list[1]

    return ch1 - ch0


def _get_stn_bm_frq_no(subarray_def, stn_bm_id, freq) -> int:
    """
    From the subarray definition,
    get the list of frequency ids for a particular station beam and return
    the index of the frequency in the list (ie 0 if first freq, etc)

    :param subarray_def: dict of subarray parameters incl station beams
    :param stn_bm_id: station-beam ID
    """
    stn_bm_list = subarray_def["stn_beams"]
    for bm_descr in stn_bm_list:
        if bm_descr["beam_id"] != stn_bm_id:
            continue
        freqs = bm_descr["freq_ids"]
        for i, frq in enumerate(freqs):
            if frq == freq:
                return i
    # oops didn't find anything - can't occur. software error
    raise RuntimeError("Couldn't find station beam frequency. Impossible!")


def _get_vis_output(subarray_def, stn_bm_id) -> dict:
    """
    From the subarray definition,
    get the list of frequency ids for a particular station beam

    :param subarray_def: dict of subarray parameters incl station beams
    :param stn_bm_id: station-beam ID
    """
    stn_bm_list = subarray_def["vis"]["stn_beams"]
    for bm_descr in stn_bm_list:
        if bm_descr["stn_beam_id"] == stn_bm_id:
            return bm_descr
    return {}


def _ipstr_2_word(ip_str: str) -> int:
    """
    Create a word representing the value of an ipv4 dotted address string
    eg "10.0.0.1" -> 0x0a000001
    """
    address_bytearray = inet_aton(ip_str)
    address_word = int.from_bytes(
        address_bytearray, byteorder="big", signed=False
    )
    return address_word


def _macstr_2_words(ip_str: str) -> (int, int):
    """
    Create words representing MAC address string
    eg "01-02-03-04-05-06" -> 0x0000102, 0x03040506
    """
    parts = ip_str.split("-")
    word0 = 0x0
    word1 = 0x0
    for count, p in enumerate(parts):
        if count < 2:
            word0 = (word0 << 8) + int(p, 16)
        else:
            word1 = (word1 << 8) + int(p, 16)
    return word0, word1


class SpeadSdp(FpgaPeripheral):
    """
    SPEAD packets out to SDP peripheral,
    for the CorrFpga FpgaPersonality
    """

    INIT_TEMPLATE_ADDR = 7168  # word offset
    TRIGGER_INIT = 1  # register bitfield value
    TRIGGER_END = 2  # register bitfield value
    TRIGGER_TIMEOUT = 3  # number of retries

    MAX_SDP_SERVERS = 16

    def enable(self):
        """
        Activate Packetiser so it can send packets
        """
        self.enable_packetiser = 0
        self.enable_packetiser = 1

    def configure_init_packets(
        self, template: bytes, first_freq_id: int, n_fine_ch: int
    ):
        """
        Configure the SPEAD initialisation packets that will be sent to SDP.

        :param template: Template packet
        :param first_freq_id: First SPS frequency ID
        :param n_fine_ch: Number of fine channels integrated in the correlation
        """
        self._set_template_packet(template)
        # floating point value, used for calculating later values
        first_freq_hz = first_visibility_freq(first_freq_id, n_fine_ch)
        self.init_hertz_base = first_freq_hz
        # FIXME this can go away when we cease using the model for the template packet
        hz_address = self.INIT_TEMPLATE_ADDR + (
            252 // 4
        )  # Luck! 252 is a multiple of 4
        # round first value to align with FPGA behaviour on later values
        self.data[hz_address] = np.array(
            np.round(first_freq_hz), dtype=ArgsWordType
        ).byteswap(inplace=True)
        self.init_hertz_increment = FINE_CHANNEL_HZ * n_fine_ch

    def _set_template_packet(self, packet: bytes):
        """
        Set the SPEAD initialisation template packet in FPGA memory.

        :param packet: SPEAD initialisation packet for FPGA to use as a
          template for generating all SPEAD initialisation packets.
        """
        padded_size = int(np.ceil(len(packet) / WORD_SIZE) * WORD_SIZE)
        data = np.zeros(padded_size, dtype=np.uint8)
        data[: len(packet)] = np.frombuffer(packet, dtype=np.uint8)
        data_to_write = data.view(dtype=ArgsWordType).byteswap(inplace=True)
        self.data[
            self.INIT_TEMPLATE_ADDR : self.INIT_TEMPLATE_ADDR
            + data_to_write.size
        ] = data_to_write
        self.spead_init_byte_count = len(packet)

        # it would be nice if we could deduce these from the packet itself...
        self.ptr_heap_counter_in_init = 15  # low byte, to 10 high byte
        # subarray: byte 10
        # beam: high 7 bits of byte 11
        # freq. channel: lowest bit of byte 11 (MSB), 12, 13 (low)
        # integration: 14 (high), 15 (low)
        self.ptr_vis_chan_id_in_init = 180  # low byte, to 183 high byte
        self.ptr_hz_field_in_init = 252  # low byte, to 255 high byte

    @property
    def init_hertz_base(self) -> IclField[float]:
        """Get init. packets base frequency."""
        integer = self.spead_init_hz_base_int.value
        fraction = self.spead_init_hz_base_frac.value
        return IclField(
            description="Initialisation packets - base frequency (Hz)",
            value=(float_from_binary_fraction(integer, fraction)),
        )

    @init_hertz_base.setter
    def init_hertz_base(self, value: float) -> None:
        """Set init. packets base frequency."""
        integer, fraction = binary_fraction(value)
        self.spead_init_hz_base_int = integer
        self.spead_init_hz_base_frac = fraction

    @property
    def init_hertz_increment(self) -> IclField[float]:
        """Get init. packets frequency increment per beam."""
        integer = self.spead_init_hz_incr_int.value
        fraction = self.spead_init_hz_incr_frac.value
        return IclField(
            description="Initialisation packets - increment per beam (Hz)",
            value=(float_from_binary_fraction(integer, fraction)),
        )

    @init_hertz_increment.setter
    def init_hertz_increment(self, value: float) -> None:
        """Set init. packets frequency increment per beam."""
        integer, fraction = binary_fraction(value)
        self.spead_init_hz_incr_int = integer
        self.spead_init_hz_incr_frac = fraction

    def _trigger_command(self, command_value: int):
        """
        Trigger SPEAD packets and wait for completion

        :param command_value: value to write to trigger register
        :raises RuntimeError: if success not observed before timeout
        """
        self.trigger_packet = 0  # reset just in case prior command failed
        self.trigger_packet = command_value
        for _ in range(self.TRIGGER_TIMEOUT):
            if self.spead_init_end_status == command_value:
                break
            time.sleep(0.1)
        else:
            # we didn't break out of for loop
            raise RuntimeError("SPEAD trigger didn't complete in time")
        # command succeeded, reset the register
        self.trigger_packet = 0

    def send_initial_packets(self, subarray_beam: int):
        """
        Send all SPEAD init packets, using current configuration settings

        :param subarray_beam: Index into subarray_beam table
        """
        self.current_subarray_beam_target = subarray_beam  # NB: register write
        self._trigger_command(self.TRIGGER_INIT)

    def send_end_packets(self, subarray_beam: int):
        """
        Send spead-end-of-stream packet
        Packetiser gets Host + UDP port via reading spead_shared_ram using
        current_subarray_beam_target value as index. Value needs to be
        written before calling here

        :param subarray_beam: Index into subarray_beam table
        """
        self.current_subarray_beam_target = subarray_beam  # NB: register write
        self._trigger_command(self.TRIGGER_END)

    def set_destination_table(self, sbt_info, subarrays_descr):
        """
        Calculate entries for the 8k-word packetiser table that contains
        IP/UDP destinations, heap counter, heap size, n_ports, freq_chans
        corresponding to each entry in the subarray_beam table

        :param sbt_info: list of subarray_beam table entries, in order
        :param subarrays_descr: dict of subarray parameters (from
               internal_subarray attribute)
        :return: 6.375kWord numpy array (to be written to packetiser RAM)
        """
        MAX_SERVERS = self.MAX_SDP_SERVERS
        ram = np.zeros(6528, dtype=np.uint32)
        for idx, entry in enumerate(sbt_info):
            (
                sa_id,
                stn_bm,
                first_coarse,
                first_fine,
                num_fine,
                i_time,
                i_chans,
                baselines,
            ) = entry  # this tuple format defined in corr_expander
            # The subarray parameters in subarrays_descr are for all subarrays
            # and the entire subarray, not just the (small) freq slice we may
            # be processing

            n_vis_chans = num_fine / i_chans

            vis_out = _get_vis_output(subarrays_descr[str(sa_id)], stn_bm)
            sdp_hosts = vis_out["host"]  # eg: [[0, '192.168.1.10'], ]
            sdp_ports = vis_out["port"]  # eg: [[0, 9000, 1], ]
            if "mac" in vis_out:
                sdp_mac = vis_out["mac"]  # eg: [[0, '02-03-04-05-06-07'], ]
                # for now just use first MAC value in list
                (
                    self.ethernet_dst_mac_u,
                    self.ethernet_dst_mac_l,
                ) = _macstr_2_words(sdp_mac[0][1])

            # number of UDP ports each SDP host receives on
            n_host_ports = 65535 - sdp_ports[0][1]
            if len(sdp_ports) > 1:
                n_host_ports = sdp_ports[1][1] - sdp_ports[0][1]
            # UDP port base guaranteed same for all hosts, pick any
            host_udp_base_port = sdp_ports[0][1]
            freq_no = _get_stn_bm_frq_no(
                subarrays_descr[str(sa_id)], stn_bm, first_coarse
            )

            # which server is first one this particular alveo sends to?
            server_no = (freq_no * 144) // n_host_ports
            first_port = host_udp_base_port + ((freq_no * 144) % n_host_ports)
            num_servers = math.ceil(n_vis_chans / n_host_ports)

            ram[0x0000 + MAX_SERVERS * idx] = _ipstr_2_word(
                sdp_hosts[server_no][1]
            )

            # logging for debug only
            ip_word = _ipstr_2_word(sdp_hosts[server_no][1])
            self._logger.info(
                "server_no=%d, num_servers=%d", server_no, num_servers
            )
            self._logger.info("first_port=%d", first_port)
            self._logger.info("sbm index=%d <- IP=0x%x", idx, ip_word)
            self._logger.info("baselines=%d", baselines)
            self._logger.info("first_fine=%d", first_fine)
            self._logger.info("num_fine=%d", num_fine)
            self._logger.info("i_chans=%d", i_chans)

            ram[0x0800 + MAX_SERVERS * idx] = first_port
            for server in range(1, num_servers):
                ram[0x0000 + MAX_SERVERS * idx + server] = _ipstr_2_word(
                    sdp_hosts[server_no + server][1]
                )
                ram[0x0800 + MAX_SERVERS * idx + server] = host_udp_base_port

            # Heap counter is combination of subarray, stn_beam IDS and chan_id
            ram[0x1000 + MAX_SERVERS * idx] = (
                (sa_id << 26) + (stn_bm << 17) + first_coarse * 144
            )
            # Heap size, 34 bytes per baseline + 8 bytes TCI
            ram[0x1800 + idx] = baselines * 34 + 8
            # Block size step per sub array (== No of ports per SDP host)
            ram[0x1880 + idx] = n_host_ports
            # no of vis frequency channels in subarray
            ram[0x1900 + idx] = n_vis_chans

        self.data = ram
