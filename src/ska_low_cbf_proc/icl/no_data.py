# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.

from ska_low_cbf_fpga import FpgaPeripheral


class NoData(FpgaPeripheral):
    """
    Peripheral that only removes the data field (if present) from
    user_attributes, as it's probably a lot of data that the control system
    doesn't care about.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "data" in self._user_attributes:
            self._user_attributes.remove("data")
