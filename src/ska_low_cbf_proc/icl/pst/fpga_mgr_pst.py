# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.
"""FPGA Manager for PST Personality"""
import struct

from ska_low_cbf_proc.icl.correlator.constants import SKA_COARSE_SPACING_GHZ
from ska_low_cbf_proc.icl.fpga_mgr import FpgaMgr
from ska_low_cbf_proc.icl.pst.pst_calcs import (
    mhz_to_first_freq_tbls,
    pst_pktzr_beam_nos,
    pst_pktzr_config,
    pst_pktzr_first_chans,
    pst_pktzr_freq_config,
    pst_vct,
)
from ska_low_cbf_proc.icl.pst.pst_fpga import PstFpga
from ska_low_cbf_proc.icl.pst_intl_iface import (
    SigProcParams,
    get_internal_scan_id,
    get_internal_subarray_id,
    get_pipeline_params,
    get_station_beam_pst_info,
    get_subarray_lists,
)
from ska_low_cbf_proc.processor.delay_subscription import DelayPolySubscriber


class PipelineState:
    """
    Class to hold parameters and state of one signal-processing pipeline
    """

    def __init__(self, params: SigProcParams):

        self.params = params  # processing parameters

        self.stn_bm_dly_sub = None  # stn-beam delay subscription
        self.stn_bm_dly_sub_name = ""  # name of dly sub attribute
        self.was_poly_upated = False
        self.stn_bm_poly_valid = False  # is stn-beam poly valid

        self.prev_vchan_pkt_nos = None  # pkt nums in used vchans
        self.highest_pkt_no = 0  # highest pkt number found so far
        self.highest_pkt_secs = 0  # seconds equivalent to pkt number
        self.highest_blk_no = 0  # 60ms filterbank block no eqiv to pkt no
        self.pkt_spread = 0  # earliest-to-latest spread of received pkt nos

        self.vct_freq_stn = None  # (freq, chan) in vct ordering


class FpgaMgrPst(FpgaMgr):
    """
    A class for PST FPGAs that monitors and programs FPGA registers

    This class translates parameters specified by the Allocator into register
    updates. It also periodically monitors FPGA registers that indicate the
    quality of the ongoing operation of the PST FPGA
    """

    PERIODIC_PRINT_SECS = 30.0
    SECS_PER_PKT = 2211840e-9  # packets have 2048 samples @ 1080ns/sample
    PKTS_PER_FILTER_FRAME = 27  # filterbank uses 27pkts (59.72msec) at a time
    FPS_NUMER = 390625  # numerator of PST filterbank frames per sec fraction
    FPS_DENOM = 23328  # denominator of PST filterbank frames per sec fraction
    POLY_IS_VALID_BIT = 0x100_0000_0000

    def __init__(
        self,
        fpga: PstFpga,
        subarray_stats_cbk,
        io_stats_cbk,
        subscr_stats_cbk,
        logger,
    ):
        super().__init__(fpga, logger)
        # override periodic timer interval for this specific FPGA personality
        self._periodic_secs = 4.0  # faster than 10-sec delay poly updates

        self.logger.info("Initialising PST FPGA interactions")
        self.fpga = fpga
        self._subarray_stats_cbk = subarray_stats_cbk
        self._io_stats_cbk = io_stats_cbk

        self._seconds = FpgaMgrPst.PERIODIC_PRINT_SECS
        self._subs_scanning = []  # list of scanning subarray_ids

        num_inst = self.fpga.num_sigproc_instances()
        self.logger.info("%d signal processing instances", num_inst)
        self._pipelines = [None] * num_inst

        self._unused_cbk = subscr_stats_cbk

    def do_register_changes(self, cmd):
        """
        Called to change FPGA register settings arising from changes to
        allocator attributes that specify the FPGA state
        :param cmd: Dictionary of parameter change commands
        """
        if "subarray" in cmd:
            subarray_descr = cmd["subarray"]
            pipeline_defs = cmd["regs"]
            self._program_regs(subarray_descr, pipeline_defs)
        else:
            self.logger.warn("Unrecognised register command")

    def periodic_actions(self):
        """
        Called to periodically check or update FPGA registers
        (period in self._periodic_secs)
        """
        self._seconds += self._periodic_secs
        if self._seconds >= FpgaMgrPst.PERIODIC_PRINT_SECS:
            self._seconds -= FpgaMgrPst.PERIODIC_PRINT_SECS
            self.logger.info("PST periodic checks")
            fpga_io_status = self.fpga.fpga_status()
            self.logger.info("fpga_status: %s", str(fpga_io_status))
            self._io_stats_cbk(fpga_io_status)
        # Update beam delay polynomials if there are scanning subarrays
        for idx, itm in enumerate(self._pipelines):
            if (
                itm is None
                or itm.params.subarray_id not in self._subs_scanning
            ):
                # pipeline not used, or subarray using is not scanning
                continue
            # These calls modify class variables, need to be done in order...
            self._check_current_pkt_counters(idx, itm)  # read SPS packet time
            self._queue_subscribed_polys(itm)  # get delays from subscriptions
            self._prep_polys_for_use(itm)  # pick the two polys to use
            self._update_delay_registers(idx, itm)  # write delay polys to FPGA
            self._check_polys_valid(
                idx, itm
            )  # check polys, enable data if valid
            # self._update_delay_stats(idx)  # publish stats about delays

    def _check_current_pkt_counters(self, idx, pipe):
        """
        Read current ingest packet counters:
            - check SPS data is being received on all in-use virtual channels
            - find highest packet number
            - for debug, log the spread of the incoming packet numbers

        :return: highest packet number seen so far on any virtual channel
        """
        all_pkt_ctrs = self.fpga.read_packet_counts(idx)
        num_vchans = len(pipe.params.freq_ids) * len(pipe.params.stn_ids)
        vchan_pkt_cntrs = all_pkt_ctrs[0:num_vchans]
        highest_pkt = max(vchan_pkt_cntrs)
        pkt_spread = 0
        if (pipe.prev_vchan_pkt_nos is not None) and (
            len(pipe.prev_vchan_pkt_nos) == num_vchans
        ):
            changes = [
                vchan_pkt_cntrs[i] - pipe.prev_vchan_pkt_nos[i]
                for i in range(0, num_vchans)
            ]
            max_delta = max(changes)
            min_delta = min(changes)
            if max_delta == 0:
                self.logger.warning(
                    "No new SPS packets (pkt_no=%s)",
                    highest_pkt,
                )
            elif min_delta == 0:
                self.logger.warning(
                    "pkt_no=%s, some missing packets",
                    highest_pkt,
                )
            else:
                # find lowest packet counter that's actually incrementing
                lowest = highest_pkt
                for cnt in range(0, num_vchans):
                    # is this virtual channel getting any packets?
                    if min_delta > 0:
                        lowest = min(lowest, vchan_pkt_cntrs[cnt])
                pkt_spread = highest_pkt - lowest
                # log recent SPS packet number info
                self.logger.debug(
                    "pkt_no=%s (spread %s)",
                    highest_pkt,
                    pkt_spread,
                )
        # save current pkt nos for comparison at next read
        pipe.prev_vchan_pkt_nos = vchan_pkt_cntrs
        # save current SPS time as highest packet count
        pipe.highest_pkt_no = highest_pkt
        # save SPS time as seconds since J2000 epoch
        pipe.highest_pkt_secs = highest_pkt * self.SECS_PER_PKT
        # save SPS time as integration_block after J2000 epoch
        pipe.highest_blk_no = highest_pkt / self.PKTS_PER_FILTER_FRAME
        # save earliest-to-latest spread in packet numbers
        pipe.pkt_spread = pkt_spread

    def _queue_subscribed_polys(self, pipe) -> None:
        """
        Drain polynomials from subscriptions
        and place in local queue
        Groom queue to contain only relevant polynomials:
        - not too old,
        - not too many,
        - no duplicates
        """
        dly_obj = pipe.stn_bm_dly_sub
        dly_name = pipe.stn_bm_dly_sub_name
        dly_obj.queue_subscribed_polys(
            pipe.highest_blk_no, self.FPS_NUMER, self.FPS_DENOM, dly_name
        )

    def _prep_polys_for_use(self, pipe) -> None:
        """
        Choose the two delay polynomials that should be
        currenly used for the station-beam
        """
        dly_obj = pipe.stn_bm_dly_sub
        dly_name = pipe.stn_bm_dly_sub_name
        pipe.was_poly_upated = dly_obj.prep_polys_for_use(
            dly_name, pipe.highest_blk_no
        )

    def _update_delay_registers(self, idx, pipe) -> None:
        """
        Write the two prepared polynomials to registers. First one
        of the two should be writen to the first hardware buffer, second
        one should be written to the second buffer.

        Both buffers will be completely written, but in some places the data
        written will be the same as the data already in the buffer

        Buffers holding data to be programmed to FPGA are initialised with
        floating-pt zero == integer zero (all zero bits)
        One 20-word entry per Virtual channel * 1024 virt chans = 20k words
        """
        if not pipe.was_poly_upated:
            return

        dly_obj = pipe.stn_bm_dly_sub

        words_buffer = [
            [0] * 20 * 1024,
            [0] * 20 * 1024,
        ]

        base_word = 0
        # get delay poly for each entry in the VCT table
        for frq_id, stn, sstn in pipe.vct_freq_stn:

            polys_avail = dly_obj.get_polys_avail()

            for buffer_num, poly in enumerate(polys_avail):
                if poly is None:
                    continue  # zero present from initialisation of buffer
                # _, blk_num, offset_sec, delay_details, _ = poly
                # find station's delay in poly
                coeffs = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                pol_offset_ns = 0
                delay_details = poly.poly
                for itm in delay_details:
                    if (
                        itm["station_id"] == stn
                        and itm["substation_id"] == sstn
                    ):
                        coeffs = itm["xypol_coeffs_ns"]
                        # pad polynomials that only have the constant term
                        while len(coeffs) < 6:
                            coeffs.append(0.0)
                        pol_offset_ns = itm["ypol_offset_ns"]
                        break
                # Collect data elements needed for FPGA programming
                sky_ghz = frq_id * SKA_COARSE_SPACING_GHZ
                offset_sec = poly.offset_sec
                blk_num = poly.first_valid_blk_no
                blk_num = blk_num | self.POLY_IS_VALID_BIT
                # convert fp and int values into 32-bit unsigned for registers
                tmp_bytes = struct.pack(
                    "<dddddddddQ",
                    *coeffs,  # no need for negation. firmware does that
                    sky_ghz,
                    offset_sec,
                    pol_offset_ns,
                    blk_num,
                )
                words_buffer[buffer_num][
                    base_word : base_word + 20
                ] = struct.unpack("<IIIIIIIIIIIIIIIIIIII", tmp_bytes)
            base_word += 20  # 20 words per delay-table entry
        self.fpga.update_beam_delay_polys(words_buffer, idx)

    def _check_polys_valid(self, idx: int, pipe):
        """
        Check whether there are valid delay polynomials and enable
        or disable the packetiser to output beam data

        :param idx: which signal processing pipeline to check
        """
        sps_pkt_time = pipe.highest_pkt_secs

        dly_obj = pipe.stn_bm_dly_sub
        polys_avail = dly_obj.get_polys_avail()
        # Delay is valid if either of the station-beam delay polys is valid
        for poly_info in polys_avail:
            if poly_info is None:
                continue
            start_time = poly_info.epoch
            valid_time = poly_info.valid_secs
            if start_time <= sps_pkt_time <= (start_time + valid_time):
                # Valid polynomial
                if not pipe.stn_bm_poly_valid:
                    self.fpga.packetiser_enable(idx, True)
                    pipe.stn_bm_poly_valid = True
                    self.logger.info("Pipe %d: Valid stn beam poly", idx)
                return
        # Delay is invalid if neither of the polys was valid
        if pipe.stn_bm_poly_valid:
            self.fpga.packetiser_enable(idx, False)
            pipe.stn_bm_poly_valid = False
            self.logger.error("Pipe %d: No valid stn beam poly", idx)

    def cleanup(self):
        """
        Final actions before thread exit
        """
        # stop any processing that may be happening in FPGA
        self._program_regs({}, [{}] * self.fpga.num_sigproc_instances())

        # clear out packet count registers
        for idx, pipe in enumerate(self._pipelines):
            # stop processing in FPGA
            self.fpga.clear_vct(idx)
            self.fpga.hbm_reset_and_wait_for_done(idx)
            self.fpga.packetiser_enable(idx, False)
            self.fpga.zero_pkt_counts(idx)  # fpga image may be re-used
            # unsubscribe from station-beam delay sources
            if pipe is not None:
                if pipe.stn_bm_dly_sub is not None:
                    pipe.stn_bm_dly_sub.unsubscribe()
                    pipe.stn_bm_dly_sub = None
            self._pipelines[idx] = None

    def _program_regs(self, intl_subarray_info: dict, intl_alveo_defs: list):
        """
        Program FPGA registers if scanning subarrays has changed.

        Note PST differs from Correlator:
            each PST pipeline handles only ONE subarray

        :param intl_subarray_info: dict of all subarray info from allocator's
            "internal_subarray" atrribute
        :param intl_alveo_defs: list of PST beam definition dictionaries
            from allocator's "internal_alveo" attribute. Each list entry
            is either empty or contains keys:
                "subarray_id"
                "pst_beam_id"
                "delay_poly"
                "jones"
                "destinations"
                ( and more: "stn_weights", "rfi_enable", ...)
        """

        subs_configured, subs_scanning_now = get_subarray_lists(
            intl_subarray_info
        )
        self.logger.info("Subarrays configured: %s", subs_configured)
        self.logger.info("Subarrays scanning: %s", subs_scanning_now)
        self.logger.info("PST pipeline descriptions: %s", intl_alveo_defs)

        # For each PST signal processing "pipeline" in FPGA ...
        for index in range(0, len(intl_alveo_defs)):
            subarray_id = get_internal_subarray_id(intl_alveo_defs, index)

            # Handle pipeline not used by any subarray
            if subarray_id is None:
                if self._pipelines[index] is None:  # already deconfigured
                    continue
                self.deconfigure_pipeline(index)
                continue

            # Configure pipeline for a new subarray(subarray_id is not None)
            if self._pipelines[index] is None:
                self.configure_pipeline(
                    intl_subarray_info, intl_alveo_defs, index
                )

            pipe = self._pipelines[index]

            # Configured subarray but not scanning, hold logic inactive
            if subarray_id not in subs_scanning_now:
                self.stop_pipeline_scan(index)
                continue

            # Configured subarray already scanning, nothing to do
            if subarray_id in self._subs_scanning:
                txt = f"PST pipeline {index} subarray {subarray_id} continues"
                self.logger.info(txt)
                continue

            # Configured subarray, neets to start scanning
            txt = f"PST pipeline {index} start for subarray {subarray_id}"
            self.logger.info(txt)
            # Ensure pipeline is stopped before making changes
            self.fpga.hbm_reset_and_wait_for_done(index)

            params = pipe.params

            # VHDL suggests this should be done before configuring. Correct??
            # self.fpga.ct2_temporary_reset(index)
            # program VCT entries
            vct_words, self._pipelines[index].vct_freq_stn = pst_vct(
                subarray_id, params.stn_bm_id, params.freq_ids, params.stn_ids
            )
            first_pkt_mhz = pst_pktzr_freq_config(params.freq_ids)
            first_chans = pst_pktzr_first_chans(params.freq_ids)
            tot_coarse = len(params.freq_ids)
            tot_stns = len(params.stn_ids)
            tot_vchans = tot_coarse * tot_stns
            self.logger.info("program VCT")
            self.fpga.prgm_vct(
                index, vct_words, tot_coarse, tot_stns, tot_vchans
            )
            self.logger.info("program VCT done")

            timing_beam_info = get_station_beam_pst_info(
                intl_subarray_info, subarray_id, params.stn_bm_id
            )
            pktzr_beam_nos = pst_pktzr_beam_nos(timing_beam_info)

            for bm in range(0, len(timing_beam_info)):
                # TODO full jones calculation from source in timing_beam_info
                # FIXME temporary fixed jones programming
                bm_jones_fixed = [
                    0x00007FFF,
                    0x00000000,
                    0x00000000,
                    0x00007FFF,
                ] * tot_vchans
                self.logger.info("program Jones beam_%d", pktzr_beam_nos[bm])
                self.fpga.write_jones_for_beam(index, bm, bm_jones_fixed)
                # FIXME temporary fixed delay programming
                bm_dly_fixed = [
                    0x00000000,
                    0x00000000,
                    0x00000000,
                    0xF333F333,  # upper or lower 16-bits has the value?
                ] * tot_vchans
                self.logger.info(
                    "program old Delays beam_%d", pktzr_beam_nos[bm]
                )
                self.fpga.write_dly_for_beam(index, bm, bm_dly_fixed)

            # configure packetiser
            words1, words2 = mhz_to_first_freq_tbls(first_pkt_mhz)
            # self.logger.info("program packetiser freq map")
            self.fpga.prgm_packetiser_first_freqs(index, words1, words2)
            self.fpga.prgm_packetiser_first_chans(index, first_chans)
            self.logger.info("program packetiser freq map done")

            scan_id = get_internal_scan_id(intl_subarray_info, subarray_id)
            pktzr_data = pst_pktzr_config(timing_beam_info, scan_id)
            # self.logger.info("program packetiser")
            self.fpga.prgm_packetiser_beam_nos(index, pktzr_beam_nos)
            self.fpga.disable_and_prgm_packetiser(index, pktzr_data)
            pipe.stn_bm_poly_valid = False
            self.logger.info("program packetiser (disabled) done")

            # configure corner turn
            n_timing_beams = len(timing_beam_info)
            # self.logger.info("program corner turn")
            self.fpga.configure_corner_turn(index, n_timing_beams)
            self.logger.info("program corner turn done")

            # Finally, start pipeline running
            # self.logger.info("start ingest")
            self.fpga.hbm_and_ingest_run(index)
            self.logger.info("start ingest done")

        self._subs_scanning = subs_scanning_now

    def stop_pipeline_scan(self, index):
        """
        Stop a pipeline processing scan data
        """
        self.logger.info("PST pipeline %s is unused", str(index))
        self.fpga.clear_vct(index)
        self.fpga.hbm_reset_and_wait_for_done(index)
        self.fpga.packetiser_enable(index, False)

    def configure_pipeline(self, intl_subarray, intl_alveo, index):
        """
        Prepare a pipeline for use by a subarray
        """
        pipe_params = get_pipeline_params(intl_alveo, intl_subarray, index)
        new_pipe = PipelineState(pipe_params)
        # Subscribe to the station-beam delay polynomial
        dev, attr = pipe_params.stn_bm_dly_src.rsplit("/", 1)
        new_pipe.stn_bm_dly_sub = DelayPolySubscriber(self.logger)
        new_pipe.stn_bm_dly_sub.subscribe(dev, attr)
        new_pipe.stn_bm_dly_sub_name = pipe_params.stn_bm_dly_src
        self.logger.info(
            "pipe %d subscribing stn beam delay %s/%s",
            index,
            dev,
            attr,
        )
        self._pipelines[index] = new_pipe

    def deconfigure_pipeline(self, index):
        """
        Pipeline no longer used by any subarray
        """
        self.fpga.clear_vct(index)
        self.fpga.hbm_reset_and_wait_for_done(index)
        self.fpga.packetiser_enable(index, False)
        if self._pipelines[index].stn_bm_dly_sub is not None:
            self._pipelines[index].stn_bm_dly_sub.unsubscribe()
            self._pipelines[index].stn_bm_dly_sub = None
            self.logger.info("pipe %d unsubscribed stn-beam delay", index)
        self._pipelines[index] = None
