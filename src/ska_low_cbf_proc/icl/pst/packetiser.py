# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

import socket

from ska_low_cbf_fpga import FpgaPeripheral, IclField


class Packetiser(FpgaPeripheral):
    """
    PSR Packetiser
    """

    CTRL_ON = 0b00000001  # Packetiser enable
    CTRL_GENERATE = 0b00000010  # Packet generation
    CTRL_GENERATE_N_RUNS = 0b00000100  # use "number of runs" parameter
    CTRL_DEFAULTS = 0b00001000  # use default packet parameters
    DATA_IP_SRC = 8
    DATA_IP_DST = 9

    _user_methods = {"on", "off", "configure"}

    @property
    def state(self) -> IclField:
        control_vector = self.control_vector.value
        if control_vector is not None:
            if control_vector & self.CTRL_ON:
                state = "On"
                if control_vector & self.CTRL_GENERATE:
                    state = "Generating Test Packets"
            else:
                state = "Off"
        else:
            state = "UNKNOWN"

        return IclField(
            description="Packetiser State", type_=str, value=state, format="%s"
        )

    def on(self):
        self.enable = True

    def off(self):
        self.enable = False

    def configure(self, generator: bool = None):
        """
        Configure the PSR Packetiser
        :param generator: Enable test packet generator
        """
        if generator is not None:
            self.generator = generator

    @property
    def enable(self) -> IclField[bool]:
        state = False
        if self.control_vector.value is not None:
            state = bool(self.control_vector.value & self.CTRL_ON)
        return IclField(
            description="PSR Packetiser Enabled",
            type_=bool,
            value=state,
            user_write=True,
        )

    @enable.setter
    def enable(self, enable_: bool):
        if self.control_vector.value is None:
            raise RuntimeError("FPGA register interface not initialised")
        if enable_:
            self.control_vector |= self.CTRL_ON
        else:
            self.control_vector &= ~self.CTRL_ON

    @property
    def generator(self) -> IclField[bool]:
        state = False
        if self.control_vector.value is not None:
            state = bool(self.control_vector.value & self.CTRL_GENERATE)
        return IclField(
            description="PSR Packet Generator",
            type_=bool,
            value=state,
            user_write=True,
        )

    @generator.setter
    def generator(self, enable: bool):
        if self.control_vector.value is None:
            raise RuntimeError("FPGA register interface not initialised")
        if enable:
            self.control_vector |= self.CTRL_GENERATE
        else:
            self.control_vector &= ~self.CTRL_GENERATE

    @property
    def default(self) -> IclField[bool]:
        state = False
        if self.control_vector.value is not None:
            state = bool(self.control_vector.value & self.CTRL_DEFAULTS)
        return IclField(
            description="PSR Default Parameters Active",
            type_=bool,
            value=state,
            user_write=True,
        )

    @default.setter
    def default(self, default_: bool):
        if self.control_vector.value is None:
            raise RuntimeError("FPGA register interface not initialised")
        if default_:
            self.control_vector |= self.CTRL_DEFAULTS
        else:
            self.control_vector &= ~self.CTRL_DEFAULTS

    @property
    def dst_ip(self) -> IclField[str]:
        """Destination IPv4 address"""
        dst_ip = self.data[self.DATA_IP_DST]
        ip = socket.inet_ntoa(int(dst_ip).to_bytes(4, "big"))
        return IclField(
            description="Destination IPv4 Address",
            type_=str,
            value=ip,
            format="%s",
            user_write=True,
        )

    @dst_ip.setter
    def dst_ip(self, ip: str):
        """Set destination IPv4 address"""
        self.data[self.DATA_IP_DST] = int.from_bytes(
            socket.inet_aton(ip), "big"
        )

    @property
    def src_ip(self) -> IclField[str]:
        """Source IPv4 address"""
        src_ip = self.data[self.DATA_IP_SRC]
        ip = socket.inet_ntoa(int(src_ip).to_bytes(4, "big"))
        return IclField(
            description="Source IPv4 Address",
            type_=str,
            value=ip,
            format="%s",
            user_write=True,
        )

    @src_ip.setter
    def src_ip(self, ip: str):
        """Set source IPv4 address"""
        self.data[self.DATA_IP_SRC] = int.from_bytes(
            socket.inet_aton(ip), "big"
        )
