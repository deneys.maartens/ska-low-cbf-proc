# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
Interface to Pulsar Timing (PST) FPGA Image
"""

import time

import numpy as np
from ska_low_cbf_fpga import FpgaPersonality
from ska_low_cbf_fpga.args_fpga import WORD_SIZE

from ska_low_cbf_proc.icl.no_attributes import NoAttributes
from ska_low_cbf_proc.icl.no_data import NoData
from ska_low_cbf_proc.icl.pst.packetiser import Packetiser


class PstFpga(FpgaPersonality):
    """
    Class providing access to Pulsar Timing (PST) FPGA by translating
    high-level operations (such as updating VCT) into lower-level register
    reads and writes

    The implementation of the class methods depends intimately on the
    register names in the FPGA VHDL, plus the function and interaction of
    the registers in the VHDL design. If the VHDL design changes, this code
    may also need to change
    """

    _peripheral_class = {
        "packetiser": Packetiser,
        "filterbanks": NoData,
        "pstbeamformer": NoData,
        "vitis_shared": NoData,
        # One ingest block for each of 3 signalprocessing paths
        "lfaadecode100g": NoData,
        # "lfaadecode100g_2": NoData,
        # "lfaadecode100g_3": NoData,
        "cmac_b": NoAttributes,
        "drp": NoAttributes,
    }

    _not_user_methods = {
        "get_vct",
        "num_sigproc_instances",
        "read_packet_counts",
    }
    """These have returns that aren't compatible with FPGA values"""

    PKTZR_EN_BIT = 0x01
    PKTZR_BEAM_TBL_BASE = 512 * WORD_SIZE
    FRQ_MAP_LO_BASE = 2048 * WORD_SIZE  # low 32-bits of freq word
    FRQ_MAP_HI_BASE = 4096 * WORD_SIZE  # hi 32-bits of freq word
    FIRST_CHAN_TBL_BASE = 6144 * WORD_SIZE  # pkt first chnl no
    POLY_BUF_SIZE = 20 * 1024 * WORD_SIZE  # 20 words per vchan
    # base addresses for PST beamformer tables
    JONES_0_BASE_BYTES = 0
    JONES_1_BASE_BYTES = 1 << 20
    DLY_0_BASE_BYTES = 2 << 20
    DLY_1_BASE_BYTES = 3 << 20

    FPGA_VCT_ENTRIES = 1024
    """Number of entries in FPGA's Virtual Channel Table"""
    WORDS_PER_VCT_ENTRY = 2
    """Number of 32-bit words for each VCT entry in FPGA"""
    INGEST_STATS_WORDS_PER_VCHAN = 8
    """Number of 32-bit words in each VCT Stats entry"""
    INGEST_STATS_PKT_CNT_OFFSET = 1
    """Which word in VCT Stats is the packet count"""
    VCT_BASE = 8192
    """VCT offset within VCSTATS RAM"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Suffixes for the 3x replicated sigproc chains in the PST ARGS/VHDL
        self._instance_suffixes = ["", "_2", "_3"]
        self._scan = [False for _ in self._instance_suffixes]

        # TODO check all expected peripherals are present

        # save vct addresses we'll access
        self._vct_addr = []  # base of VirtualChannelTables
        self._vcstats_addr = []  # base of VC stats
        self._table_addr = []  # base of 4 adj regs (tbl, stns, coarse, vchans)
        self._lfaa_reset_addr = []
        self._hbm_status_addr = []
        self._hbm_reset_addr = []
        # save packetiser addresses
        self._pktzr_cfg_addr = []
        self._pktzr_en_addr = []  # packetiser control vector
        self._ct_out_beamsenabled = []
        self._stn_beam_poly_addr = []

        self._stns_addr = []  # base of 3 adjacent regs (stns, coarse, chans)
        self._ct_reset_addr = []

        self._beamformer_addr = []  # PST beam boresight offset phasing

        for suffix in self._instance_suffixes:
            periph_name = f"lfaadecode100g{suffix}"
            periph = self[periph_name]

            self._vcstats_addr.append(periph["data"].address)
            vct_addr = periph["data"].address + WORD_SIZE * self.VCT_BASE
            self._vct_addr.append(vct_addr)
            self._table_addr.append(periph["table_select"].address)
            self._lfaa_reset_addr.append(periph["lfaa_decode_reset"].address)
            self._hbm_status_addr.append(periph["hbm_reset_status"].address)
            self._hbm_reset_addr.append(periph["hbm_reset"].address)
            self._stns_addr.append(periph["total_stations"].address)

            periph_name = f"pst_ct1{suffix}"
            periph = self[periph_name]
            self._ct_reset_addr.append(periph["full_reset"].address)
            self._stn_beam_poly_addr.append(periph["data"].address)

            periph_name = f"pstbeamformer{suffix}"
            periph = self[periph_name]
            self._beamformer_addr.append(periph["data"].address)

            periph_name = f"ct_atomic_pst_out{suffix}"
            periph = self[periph_name]
            self._ct_out_beamsenabled.append(periph["beamsenabled"].address)

            periph_name = f"packetiser{suffix}"
            periph = self[periph_name]
            self._pktzr_en_addr.append(periph["control_vector"].address)
            self._pktzr_cfg_addr.append(periph["data"].address)

        # Explicitly reset pipelines in case we've inherited an FPGA
        # image that wasn't actually reloaded from the bitfile
        for idx in range(0, len(self._instance_suffixes)):
            self.hbm_reset_and_wait_for_done(idx)
            self.disable_packetiser(idx)

        # Check vct register-adjacency assumptions
        assert self.lfaadecode100g.total_coarse.address == (
            self.lfaadecode100g.total_stations.address + WORD_SIZE
        ), "Registers no longer adjacent: total_stations, total_coarse"
        assert self.lfaadecode100g.total_channels.address == (
            self.lfaadecode100g.total_coarse.address + WORD_SIZE
        ), "Registers no longer adjacent: total_coarse, total_channels"

        # initialise shadow copy of VCT info in FPGA registers
        self._vcts = self.get_vct(access_hw=True)

    def __del__(self):
        "Cleanup as Alveo card may be left locked when using XRT driver"
        super().__del__()

    def num_sigproc_instances(self) -> int:
        """Get number of signal processing pipelines in FPGA image"""
        return len(self._instance_suffixes)

    def get_vct(self, access_hw=False) -> list:
        """
        Read current VCT, from FPGA registers or from shadow
        :return: list of VCTs entries=[subarray, beam, freq, stn, sstn]
        """
        if not access_hw:
            return self._vcts

        # Read each VCT from HW regs, translate to lists
        vcts = []
        for idx in range(0, len(self._instance_suffixes)):
            # check size of VCT
            stns_addr = self._stns_addr[idx]
            stns, chans = self._driver.read(stns_addr, 2)
            vct_size = stns * chans
            if vct_size == 0:
                vcts.append([])
                continue

            # limit readback from FPGA to the max size of VCT
            if vct_size > self.FPGA_VCT_ENTRIES:
                vct_size = self.FPGA_VCT_ENTRIES

            vct_words = vct_size * self.WORDS_PER_VCT_ENTRY

            vct_addr = self._vct_addr[idx]
            vct_raw = self._driver.read(vct_addr, vct_words)
            vct_lst = []
            for idx in range(0, len(vct_raw), self.WORDS_PER_VCT_ENTRY):
                val = vct_raw[idx]
                subarray = (val >> 16) & 0x01F
                beam = (val >> 9) & 0x0F
                freq = val & 0x0FF
                stn = (val >> 21) & 0x03FF
                sstn = (val >> 13) & 0x07
                vchan = vct_raw[idx + 1]
                vct_lst.append([stn, sstn, subarray, beam, freq, vchan])
            vcts.append(vct_lst)
        return vcts

    def prepare_scan(
        self,
        stn_beam_dly_srcs: str,
        pst_beam_list: list,
        stn_weights: list,
        delay_srcs: list,
    ) -> None:
        """
        Make all the register changes required in preparation for a scan

        :param stn_beam_dly_srcs: Tango FQDN for subscription to boresight
        delay polynomial of the station beam from which the PST beams are
        calculated
        :param pst_beam_list:  List of PST beam IDs to be generated
        :param stn_weights: List of station weights, one list entry per PST
        beam, same order as previous parameter
        :param delay_srcs: List of Tango FQDNs for subscription to PST beam
        delay polynomials, one entry per PST beam, same order as previous
        parameters
        """
        for sig_proc_idx, pst_beams in enumerate(pst_beam_list):
            # update boresight delays from subscription to stn_beam_dly_src
            # TODO delay poly should come from dly src for the pst beam
            # TODO which stn_beam_dly_srcs entry applies to this sig_proc inst?
            # init_dly_poly = [0, 0, 0, 2 * 1080.0e-9]  # FIXME

            # TODO add in delay subscription object
            # self.write_stn_beam_delays(sig_proc_idx, init_dly_poly)

            beam_count = 0
            for pst_beam in pst_beams:
                jones = []
                offset_dly = []
                for vct_entry in self._vcts[sig_proc_idx]:
                    stn, sstn, sbry, stnbm, freq_id, vchan = vct_entry
                    # TODO lookup subscription jones val for the vct entry
                    # - use constants until we have subscription
                    # unity Jones and weight
                    jvals = [0x00007FFF, 0x00000000, 0x00000000, 0x00007FFF]
                    jones.extend(jvals)
                    # TODO lookup subscription pst delay for the vct entry
                    # - use constants until we have subscription
                    odly = [0x00000000, 0x00000000, 0x00000000, 0xF333F333]
                    offset_dly.extend(odly)
                # write Jones + weights
                self.write_jones(sig_proc_idx, beam_count, jones)
                # write PST beam offset delays
                self.write_beam_wt_dly(sig_proc_idx, beam_count, offset_dly)
                # write station beam id
                self.write_pst_beam_id(sig_proc_idx, beam_count, pst_beam)
                beam_count += 1
            # write number of beams = beam_count
            n_beams = np.asarray([beam_count], dtype=np.uint32)
            self._driver.write(self._beamformer_addr[sig_proc_idx], n_beams)

        # TODO write remapper

    def update_scan(self, cmd_wi_str_keys) -> None:
        """
        Perform register changes required to start/stop sending PST beams

        :param cmd_wi_str_keys: is dict of scan status,
        ie {subarray_id:True/False} but subarray_id key will have been
        converted to string by JSON
        """
        # convert string keys back to integer IDs
        cmd = {}
        for key, val in cmd_wi_str_keys.items():
            cmd[int(key)] = val
        # Start/stop sending data as required by subarray scan status
        for count, subarray_id in enumerate(self._subarray_id):
            if subarray_id not in cmd:
                continue
            should_scan = cmd[subarray_id]
            if should_scan != self._scan[count]:
                txt = f"Register update: scan={should_scan}"
                txt += f" for subarray={subarray_id}"
                txt += f", inst={count}"
                self._logger.info(txt)

                if should_scan:
                    # full reset of corner turn
                    rst = np.asarray([1], dtype=np.uint32)
                    self.driver.write(self._pktzr_en_addr[count], rst)
                    clr = np.asarray([0], dtype=np.uint32)
                    self.driver.write(self._pktzr_en_addr[count], clr)
                    # enable packetiser output
                    ctrl = np.asarray([1], dtype=np.uint32)
                    self.driver.write(self._pktzr_en_addr[count], ctrl)
                else:
                    # disable packetiser output
                    ctrl = np.asarray([0], dtype=np.uint32)
                    self.driver.write(self._pktzr_en_addr[count], ctrl)

                self._scan[count] = should_scan

    def write_beam_wt_dly(
        self, sigproc_inst_no: int, pst_beam_count: int, phase_vals
    ):
        """
        Write PST beam offset phasing for sigproc and beam

        :param pst_beam_count: PST beam to program, range 0 to N_PST_BEAMS-1
        :param phase_vals: list of phase constants calculated from delay poly
        """
        dvals = np.asarray(phase_vals, dtype=np.uint32)

        # Delay values are two 16k blobs (two ping-pong buffers) starting at
        # byte 32768 or 49152 in each 64k buffer belonging to a beam
        buffer_no = 0  # TODO should ping-pong 0,1, after reading register
        offset = pst_beam_count * 65536 + 32768 + (buffer_no * 16384)

        self.driver.write(
            self._beamformer_addr[sigproc_inst_no] + offset, dvals
        )

    def write_jones_for_beam(
        self, sigproc_inst_no: int, pst_beam_idx: int, jones_vals
    ):
        """
        Write Jones values for sigproc and beam

        :param pst_beam_count: PST beam to program, range 0 to N_PST_BEAMS-1
        :param jones_vals: Jones constants (incl weights) to program
        """
        jvals = np.asarray(jones_vals, dtype=np.uint32)
        # write to first jones buffer (offset 0Mbyte)
        offset = self.JONES_0_BASE_BYTES + pst_beam_idx * 1024 * 4 * 4
        self.driver.write(
            self._beamformer_addr[sigproc_inst_no] + offset, jvals
        )
        # write same to second jones buffer (offset 1Mbyte)
        offset = self.JONES_1_BASE_BYTES + pst_beam_idx * 1024 * 4 * 4
        self.driver.write(
            self._beamformer_addr[sigproc_inst_no] + offset, jvals
        )

    def write_dly_for_beam(
        self, sigproc_inst_no: int, pst_beam_idx: int, dly_vals
    ) -> None:
        """
        Write Delay values for sigproc and beam. This will be replaced
        when the FPGA is updated with registers for PST polynomial

        :param pst_beam_count: PST beam to program, range 0 to N_PST_BEAMS-1
        :param dly_vals: Delay constants (incl weights) to program
        """
        dvals = np.asarray(dly_vals, dtype=np.uint32)
        # write to first delay buffer (offset 2Mbyte)
        offset = self.DLY_0_BASE_BYTES + pst_beam_idx * 1024 * 4 * 4
        self.driver.write(
            self._beamformer_addr[sigproc_inst_no] + offset, dvals
        )
        # write same to second delay buffer (offset 3Mbyte)
        offset = self.DLY_1_BASE_BYTES + pst_beam_idx * 1024 * 4 * 4
        self.driver.write(
            self._beamformer_addr[sigproc_inst_no] + offset, dvals
        )

    def write_jones(
        self, sigproc_inst_no: int, pst_beam_count: int, jones_vals
    ) -> None:
        """
        Write Jones values for sigproc and beam

        :param pst_beam_count: PST beam to program, range 0 to N_PST_BEAMS-1
        :param jones_vals: Jones constants (incl weights) to program
        """
        jvals = np.asarray(jones_vals, dtype=np.uint32)

        # Jones values are a 16kbyte blob starting at byte 0 or 16384 in each
        # beam's 64kbyte buffer
        buffer_no = 0  # TODO should ping-pong 0,1 after reading register
        offset = pst_beam_count * 65536 + (buffer_no * 16384)

        self.driver.write(
            self._beamformer_addr[sigproc_inst_no] + offset, jvals
        )

    def write_pst_beam_id(
        self, sigproc_inst_no: int, pst_beam_count: int, pst_beam_id
    ):
        """TODO  - which register does beam id go into?"""
        pass

    def get_sky_freq_list(self, sig_proc_inst: int):
        """
        For each Virtual ChannelTable (VCT) Calculate a sky frequency list

        :param: signal processing instance number
        :return: list of lists with frequencies in Hz
        """
        assert (
            0 <= sig_proc_inst < len(self._instance_suffixes)
        ), f"signal processing instance no {sig_proc_inst} out of range"

        SKY_CHAN_SPACING_HZ = 800e6 / 1024  # Coarse channel spacing

        # determine list of channel freqs from VCT
        freq_list_hz = []
        for _, _, _, _, freq_id, _ in self._vcts[sig_proc_inst]:
            freq_list_hz.append(freq_id * SKY_CHAN_SPACING_HZ)
        return freq_list_hz

    def get_vct_station_list(self, sig_proc_inst: int):
        """
        For each Virtual ChannelTable (VCT) Calculate extract
        station/substation tuple for each VCT entry, in order
        :param: signal processing instance number
        :return: list of (station,substation) tuples
        """
        assert (
            0 <= sig_proc_inst < len(self._instance_suffixes)
        ), f"signal processing instance no {sig_proc_inst} out of range"

        # determine list of station/substations from VCT
        all_stations = []
        for stn, sstn, _, _, _, _ in self._vcts[sig_proc_inst]:
            all_stations.append((stn, sstn))
        return all_stations

    def configure_corner_turn(self, idx, n_beams) -> None:
        """
        # TODO FIX this code skeleton
        """
        self._driver.write(self._ct_out_beamsenabled[idx], n_beams)

    def disable_packetiser(self, idx) -> None:
        """
        Disable a particular pipeline's packetiser

        :param idx: which pipeline to disable [0,1,2]
        """
        enable_word = 0x0
        self._driver.write(self._pktzr_en_addr[idx], enable_word)

    def prgm_packetiser_first_freqs(
        self, idx: int, words1: list[int], words2: list[int]
    ) -> None:
        """
        Write new VCT-to-freq mapping tables for a pipeline

        Two tables, containing 1024 32-bit entries
        :param idx: which signal-processing pipeline to update
        """
        w1_as_np = np.asarray(words1, dtype=np.uint32)
        self.driver.write(
            self._pktzr_cfg_addr[idx] + self.FRQ_MAP_LO_BASE, w1_as_np
        )
        w2_as_np = np.asarray(words2, dtype=np.uint32)
        self.driver.write(
            self._pktzr_cfg_addr[idx] + self.FRQ_MAP_HI_BASE, w2_as_np
        )

    def prgm_packetiser_beam_nos(self, idx: int, datablob: list[int]) -> None:
        """
        write new packetiser beam-numbers table (16 values) for a pipeline
        :param idx: which signal-processing pipeline to update [0,1,2]
        """
        data_as_np = np.asarray(datablob, dtype=np.uint32)
        self._driver.write(
            self._pktzr_cfg_addr[idx] + self.PKTZR_BEAM_TBL_BASE, data_as_np
        )

    def prgm_packetiser_first_chans(
        self, idx: int, chan_nos: list[int]
    ) -> None:
        """
        write new packetiser first-channel-no table (16 values) for a pipeline
        :param idx: which signal processing pipeline to update [0,1,2]
        """
        data_as_np = np.asarray(chan_nos, dtype=np.uint32)
        self._driver.write(
            self._pktzr_cfg_addr[idx] + self.FIRST_CHAN_TBL_BASE, data_as_np
        )

    def disable_and_prgm_packetiser(
        self, idx: int, datablob: list[int]
    ) -> None:
        """
        Disable packetiser and write new packetiser params for a pipeline
        :param idx: which pipeline to update [0,1,2]
        """
        self._driver.write(self._pktzr_en_addr[idx], 0x0)
        data_as_np = np.asarray(datablob, dtype=np.uint32)
        self._driver.write(self._pktzr_cfg_addr[idx], data_as_np)

    def packetiser_enable(self, idx: int, enable: bool) -> None:
        """
        Enable or disable packetiser.
        FPGA requires a rising edge to enable & load new parameters

        :param idx: which pipeline's packetiser to enable
        """
        self._driver.write(self._pktzr_en_addr[idx], 0x0)
        if enable:
            self._driver.write(self._pktzr_en_addr[idx], self.PKTZR_EN_BIT)

    def ct2_temporary_reset(self, idx: int) -> None:
        """
        Apply full reset to corner turn 2 to reset its logic and
        pick up any change to channels/stations

        :param idx: which signal processing pipeline [0..2]
        """
        self._driver.write(self._ct_reset_addr[idx], 0x1)
        self._driver.write(self._ct_reset_addr[idx], 0x0)

    def prgm_vct(
        self, idx, vct_words, tot_coarse, tot_stns, tot_vchans
    ) -> None:
        """
        Write a new VCT and associated params to a PST pipeline

        :param idx: PST firmware pipeline number
        :param vct_words: list of VCT entry values
        :param tot_coarse: number of coarse channels beamformed
        :param tot_stns: number of (sub-)stations beamformed
        :param tot_vchans: number of FPGA virtual channels used
        """
        # which VCT is unused ?
        tbl = self._driver.read(self._table_addr[idx], 1)
        nxt_tbl = tbl ^ 1
        nxt_tbl_byte_ofset = (
            nxt_tbl
            * self.FPGA_VCT_ENTRIES
            * self.WORDS_PER_VCT_ENTRY
            * WORD_SIZE
        )
        # Write settings to unused VCT
        vct_as_np = np.asarray(vct_words, dtype=np.uint32)
        self._driver.write(self._vct_addr[idx] + nxt_tbl_byte_ofset, vct_as_np)
        # Temporarily (@ 2024-02-22) use single-register writes for these regs
        self._driver.write(self._table_addr[idx] + 1 * WORD_SIZE, tot_stns)
        self._driver.write(self._table_addr[idx] + 2 * WORD_SIZE, tot_coarse)
        self._driver.write(self._table_addr[idx] + 3 * WORD_SIZE, tot_vchans)
        self._driver.write(self._table_addr[idx], nxt_tbl)
        # When FPGA is fixed, use single write:
        # data = np.asarray(
        #     [nxt_tbl, tot_stns, tot_coarse, tot_vchans], dtype=np.uint32
        # )
        # self._driver.write(self._table_addr[idx], data)

    def clear_vct(self, idx) -> None:
        """
        Make all VCT entries invalid (so any SPS input is rejected)
        :param idx: which signal processing pipeline to clear 0..2
        """
        self.prgm_vct(idx, [0] * self.FPGA_VCT_ENTRIES, 0, 0, 0)

    def hbm_reset_and_wait_for_done(
        self, idx: int, sleep_secs=0.005, timeout_count=100
    ) -> None:
        """
        Hold HBM & Ingest in reset to allow VCT modifications to be made

        :param idx: PST firmware pipeline number
        """
        self.driver.write(self._hbm_reset_addr[idx], 0x1)  # reset HBM
        cnt = 0
        while (self.driver.read(self._hbm_status_addr[idx]) != 0x1) and (
            cnt < timeout_count
        ):
            time.sleep(sleep_secs)
            cnt += 1
        if cnt >= timeout_count:
            self._logger.error("HBM reset did not complete")
        # This comes after HBM reset so HBM has data flow to reach reset state
        self.driver.write(self._lfaa_reset_addr[idx], 0x1)  # reset ingest

    def hbm_and_ingest_run(self, idx: int) -> None:
        """
        Enable pipeline to start producing PST beam output after a config change

        :param idx: PST firmware pipeline number
        """
        # This comes first so HBM is ready to accept ingest data
        self.driver.write(self._hbm_reset_addr[idx], 0x0)  # run HBM
        self.driver.write(self._lfaa_reset_addr[idx], 0x0)  # run ingest

    def fpga_status(self) -> dict:
        """
        Gather status indication for FPGA processing operations
        """
        summary = {}
        summary["fpga_uptime"] = self["system"]["time_uptime"].value
        summary["total_pkts_in"] = self["system"][
            "eth100g_rx_total_packets"
        ].value
        summary["spead_pkts_in"] = self["lfaadecode100g"][
            "spead_packet_count"
        ].value
        summary["total_pkts_out"] = self["system"][
            "eth100g_tx_total_packets"
        ].value
        # Total good pkt output across all signal processing pipelines
        total_of_good_pkts = 0
        pipe_names = ["packetiser", "packetiser_2", "packetiser_3"]
        for pipe_no in range(0, len(pipe_names)):
            total_of_good_pkts += self[pipe_names[pipe_no]][
                "stats_packets_rx_sig_proc_valid"
            ].value
        summary["pst_pkts_out"] = total_of_good_pkts
        return summary

    def read_packet_counts(self, idx):
        """
        Read most recent packet count received for each VirtualChannel
        in the particular signal processing pipeline

        :param idx: which signal processing pipeline 0..2

        :return: list of counters, one per virtual chan
        """
        n_words = self.FPGA_VCT_ENTRIES * self.INGEST_STATS_WORDS_PER_VCHAN
        pkt_cnts = self._driver.read(self._vcstats_addr[idx], n_words)
        return pkt_cnts[
            self.INGEST_STATS_PKT_CNT_OFFSET : n_words : self.INGEST_STATS_WORDS_PER_VCHAN
        ].tolist()

    def zero_pkt_counts(self, idx):
        """Clear packet counters in FPGA registers"""
        n_words = self.FPGA_VCT_ENTRIES * self.INGEST_STATS_WORDS_PER_VCHAN
        np_data = np.zeros(n_words, dtype=np.uint32)
        self.driver.write(self._vcstats_addr[idx], np_data)

    def update_beam_delay_polys(self, buffer_pair: list, idx) -> None:
        """
        Write delay polynomial data into firmware registers

        :param buffer_pair: Two-element list, each element a 20kword list
        :param idx: which pipeline is to be updated
        of integers to be written to FPGA delay polynomial buffers as uint32
        """
        for cnt, buffer in enumerate(buffer_pair):
            self.driver.write(
                self._stn_beam_poly_addr[idx] + self.POLY_BUF_SIZE * cnt,
                np.asarray(buffer, dtype=np.uint32),
            )
        return


def calc_delay(
    delay_poly: tuple, p_time: int, coarse_freq_hz: float, offset_ns: float = 0
) -> list:
    """
    Calculate PST coarse-channel delay programming from delay polynomial
    See low-cbf-model, getPSTRegisterSettings.m, lines 258-315

    :param: delay_poly Tuple of 4 values, coefficients of a cubic polynomial
    :param: p_time Time in seconds at which the polynomial is to be evaluated.
    Time value must correspond to the first sample in
    the Coarse Corner Turner (CTC) because delay values are updated only on
    CTC buffer swaps (There are 408 packets of 2048 samples each in a corner
    turn buffer) The argument must take into account the offset between the
    polynomial zero time and the time in the SPS packets
    :param: coarse_freq_hz centre frequency of the coarse channel
    :param: offset_ns Sampling offset between the two polarisations
    :param: the sampling delay between the two RF polarisations (nanoseconds)
    :return: tuple of 4x32-bit register values for programming
    """
    SAMPLE_PERIOD = 1080.0e-9  # SPS: 1080 nsec/sample (exact)
    SAMPLE_RATE = 1.0 / SAMPLE_PERIOD
    NYQUIST_FREQ = SAMPLE_RATE * 0.5
    SAMPLES_PER_PHASE_UPDATE = 64
    MAX_COARSE_DELAY = 2048
    TWO_POW15 = 32786
    TWO_POW16 = 65536
    TWO_POW30 = 1073741824
    TWO_POW32 = 4294967296

    aa, bb, cc, dd = delay_poly
    delay_secs = ((aa * p_time + bb) * p_time + cc) * p_time + dd

    # Whole-samples delay
    # (Implemented by varying the read point in the SPS input data)
    dly_samples = round(delay_secs * SAMPLE_RATE)
    assert (
        0 < dly_samples < MAX_COARSE_DELAY
    ), f"delay {dly_samples} outside [1-{MAX_COARSE_DELAY-1}]"
    # Fractional-sample delay
    # (Implemented as a phase shift to fine filterbank channel outputs)
    dly_frac_h = delay_secs - dly_samples * SAMPLE_PERIOD  # +/- 0.5
    dly_frac_v = dly_frac_h + offset_ns * 1e-9

    # PHI_SHIFT = (coarse_centre_freq + NYQUIST_FREQ) * frac_sample_dly_secs

    # Phase shift is programmed in two parts to allow FPGA to compute the
    # delay appropriate to each fine channel (different frequency)
    # (1) part due to offset from centre freq of coarse channel,
    #     calculated at f=Nyquist (and the FPGA scales for each fine channel)
    # (2) part due to centre freq of coarse channel
    #
    # Delay changes over time. Calculated from the slope of the delay
    # polynomial. Phase step size (above) is adjusted every 64 samples

    # FPGA implements phase shift as a 16 twos-complement value:
    # 1 sign bit, (12 bit integer part, 3 bit fraction) = 15 bits mantissa
    phase_bits = round(NYQUIST_FREQ * dly_frac_h * TWO_POW15)
    phase_bits_2c16 = twos_comp_16bit(phase_bits)
    phase_bits_v = round(NYQUIST_FREQ * dly_frac_v * TWO_POW15)
    phase_bits_2c16_v = twos_comp_16bit(phase_bits_v)

    # first register: H-pol phase delay and whole sample delay
    w0 = (phase_bits_2c16 << 16) + twos_comp_16bit(
        MAX_COARSE_DELAY - dly_samples
    )

    # Phase shift is updated every 64 samples
    delay_rate = (3 * aa * p_time + 2 * bb) * p_time + cc  # d/dt of cubic
    rate_bits = round(
        NYQUIST_FREQ
        * delay_rate
        * (SAMPLES_PER_PHASE_UPDATE * SAMPLE_PERIOD)
        * TWO_POW30
    )
    # TODO assert not overflowing 16-bit 2s complement range??
    rate_bits_2c16 = twos_comp_16bit(rate_bits)

    # second register: phase-rate update per 64 samples, and V-pol phase delay
    w1 = (rate_bits_2c16 << 16) + phase_bits_2c16_v

    # phase offset due to coarse channel centre frequency
    offset_phase_cycles = delay_secs * coarse_freq_hz
    offset_phase_cycle_frac = offset_phase_cycles - int(offset_phase_cycles)
    offset_phase_bits = round(offset_phase_cycle_frac * TWO_POW16)
    if offset_phase_bits >= TWO_POW16:
        offset_phase_bits = 0
    offset_phase_cyc_v = (delay_secs + offset_ns * 1e-9) * coarse_freq_hz
    offset_phase_cyc_frac_v = offset_phase_cyc_v - int(offset_phase_cyc_v)
    offset_phase_bits_v = round(offset_phase_cyc_frac_v * TWO_POW16)
    if offset_phase_bits_v >= TWO_POW16:
        offset_phase_bits_v = 0
    # third reg: H-pol,V-pol phase offset due to coarse channel centre freq
    w2 = (offset_phase_bits_v << 16) + offset_phase_bits

    # phase step due to channel frequency, applied every 64 samples
    delta_delay = delay_rate * SAMPLES_PER_PHASE_UPDATE * SAMPLE_PERIOD
    delta_cycles = delta_delay * coarse_freq_hz
    delta_cycles_frac = delta_cycles - int(delta_cycles)
    delta_cycles_frac_bits = delta_cycles_frac * TWO_POW32
    if delta_cycles_frac_bits >= TWO_POW32:
        delta_cycles_frac_bits = 0
    # fourth register: phase step (due to centre freq) per 64 samples
    w3 = round(delta_cycles_frac_bits)

    return [w0, w1, w2, w3]  # four 32-bit register values


def twos_comp_16bit(val: int) -> int:
    """return a 16-bit 2s complement version of input"""
    TWO_POW16 = 65536
    TWO_POW15 = 32768
    # Saturate value at 16-bit 2s complement limits [-2**15, (2**15)-1]
    if val >= TWO_POW15:
        return TWO_POW15 - 1
    if val < -TWO_POW15:
        val = -TWO_POW15
    # Adjust negative values into correct 2s complement range
    if val < 0:
        return val + TWO_POW16
    return val
