# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Functions to extract PST information from allocator attribute data

This file exists to confine references to specific field names
in the internal_alveo and internal_subarray attributes to a single file
"""

from dataclasses import dataclass


def get_subarray_lists(subarray_info):
    """
    From global subarray information (published by allocator), extract
    - sorted list of all subarray_ids (configured subarrays, scanning or not)
    - sorted list of scanning subarray_ids (subset of above)
    """
    all_subarray_ids = set()
    scanning_subs = set()
    for (
        subarray_id_txt,
        subarray_description,
    ) in subarray_info.items():
        subarray_id = int(subarray_id_txt)
        all_subarray_ids.add(subarray_id)

        if subarray_description["scanning"]:
            scanning_subs.add(subarray_id)
    return sorted(list(all_subarray_ids)), sorted(list(scanning_subs))


@dataclass
class SigProcParams:
    """
    Structure describing signal processing parameters for one PST pipeline.
    It contains only information that is constant for the duration of a subarray
    configuration. It does not (cannot) include scan_id or scanning status which
    change from one scan to another during a subarray configuration
    """

    subarray_id: int
    """subarray ID"""
    stn_bm_id: int
    """station beam id"""
    freq_ids: list[int]
    """list of frequency IDs in station beam"""
    stn_ids: list[tuple]
    """ list of (station, substation) tuples for station beam"""
    stn_bm_dly_src: str
    """Station beam delay polymial source, Tango URI"""
    pst_bm_ids: list[int]
    """List of PST beam IDs formed from station beam"""
    pst_dly_srcs: list[str]
    """List of delay polynomial sources (Tango URI) for each PST beam"""
    jones_srcs: list[str]
    """List of Jones sources (Tango URI) for each PST beam"""


@dataclass
class PstDest:
    """Structure describing a server destination for PST beam packets"""

    ip_addr: str
    """IP address to send to eg '10.1.2.3' """
    udp_port: int
    """ UDP port number to send to"""
    start_chan: int
    """ First PST channel sent to this host"""
    num_chans: int
    """ Number of subsequent channels sent to this host"""


@dataclass
class PstBeam:
    """Structure describing a PST beam"""

    pst_beam_id: int
    """PST beam_ID (expect [1..16])"""
    delay_poly_uri: str
    """Tango subscription URI for PST delays"""
    jones_uri: str
    """ Tango subscription URI for Jones matrix"""
    server_dests: list
    """ List of PstDest objects (expect 1 or 2 only)"""


def get_station_beam_pst_info(intl_subarray, subarray_id, beam_id):
    """
    From subarray info published by allocator internal_subarray attribute,
    extract info about the PST beams in for a particular subarray station-beam

    :param intl_subarray: the value published by the allocator's
    internal_subarray attribute
    :param subarray_id: ID of the subarray
    :param beam_id: station_beam ID
    """
    subarray_pst_beams = intl_subarray[str(subarray_id)]["timing_beams"][
        "beams"
    ]
    stn_bm_pst_beams = []
    for beam in subarray_pst_beams:
        if beam["stn_beam_id"] != beam_id:
            continue
        server_dests = []
        for itm in beam["destinations"]:
            dest = PstDest(
                itm["data_host"],
                itm["data_port"],
                itm["start_channel"],
                itm["num_channels"],
            )
            server_dests.append(dest)
        bm = PstBeam(
            beam["pst_beam_id"],
            beam["delay_poly"],
            beam["jones"],
            server_dests,
        )
        stn_bm_pst_beams.append(bm)
    return stn_bm_pst_beams


def get_internal_scan_id(subarray_info, subarray_id):
    """
    From internal_subarray attribute value published by the allocator,
    extract scan_id (if any) for selected subarray_id

    :param subarray_info: the value published by the allocator's
    internal_subarray attribute
    :param subarray_id: ID of the subarray being scanned

    [Note: scan_id can't be part of the SigProcParams returned by
    get_pipeline_params below, since the latter is called when a subarray is
    configured, but scan_id changes after configuration from one scan to
    another. scan_id has to be extracted each time from subarray_info]

    """
    scan_id = subarray_info[str(subarray_id)].get("scan_id", 0)
    return scan_id


def get_internal_subarray_id(intl_alveo, idx):
    """
    From internal_alveo attribute data published by allocator, extract
    the subarray ID associated with a pipeline identified by its index [0..2]

    :param intl_alveo: the value published by the allocator's
    internal_alveo attribute

    [Note: subarray_id has to be extracted from the internal_alveo value
    published by the allocator - to see if it has changed. It cannot be
    obtained from SigProcParams that reflect existing configuration]
    """
    if idx > len(intl_alveo):
        return None
    # Allow for the entry to be an empty dictionary if pipeline is unused
    subarray_id = intl_alveo[idx].get("subarray_id", None)
    return subarray_id


def subarray_beams_as_dict(subarray_id, intl_subarray) -> dict:
    """
    Extract description of a selected subarray from the global subarray info

    :param subarray_id: the subarray ID
    :param intl_subarray: value of the allocator internal_subarray attribute
    :return: dict describing the subarray station beams and their pst beams
    """
    subarray_info = intl_subarray[str(subarray_id)]

    stn_beams = {}
    # Add station beams to dict, key=station_beam_id
    for bm in subarray_info["stn_beams"]:
        stn_beams[bm["beam_id"]] = {
            "freq_ids": bm["freq_ids"],
            "delay_poly": bm["delay_poly"],
            "timing_beams": {},  # Data added in loop below
        }
    # Add timing beams into appropriate station beam
    for bm in subarray_info["timing_beams"]["beams"]:
        stn_bm_id = bm["stn_beam_id"]
        if stn_bm_id not in stn_beams:
            # User specified a pst beam in an unknown station beam. Ignore
            continue
        stn_beams[stn_bm_id]["timing_beams"][bm["pst_beam_id"]] = {
            "pst_poly": bm["delay_poly"],
            "jones": bm["jones"],
            "destinations": bm["destinations"],
        }
    return stn_beams


def get_pipeline_params(intl_alveo, intl_subarray, idx) -> tuple:
    """
    From internal_alveo and subarray attributes extract info describing
    the processing in a PST pipeline

    :param intl_alveo: the internal_alveo definition for this FPGA
    :param intl_subarray: subarray descriptions
    :param idx: which internal_alveo pipeline we want info about
    :return: tuple of data about this pipeline's processing (see below)
    """
    # parameters we can get from the pipeline def (internal_alveo)
    subarray_id = intl_alveo[idx]["subarray_id"]
    stn_bm_id = intl_alveo[idx]["stn_bm_id"]  # stn beam for this pipeline
    freq_ids = intl_alveo[idx]["freq_ids"]  # freqs this FPGA processes
    sstns = intl_alveo[idx]["stns"]  # same info exists in subarray def
    pst_bm_ids = intl_alveo[idx]["pst_bm_ids"]

    pst_dly_srcs = [None] * len(pst_bm_ids)
    jones_srcs = [None] * len(pst_bm_ids)
    # parameters we need to get from subarray def (internal_subarray)
    stn_beams = subarray_beams_as_dict(subarray_id, intl_subarray)
    assert (
        stn_bm_id in stn_beams
    ), f"Error in subarray.configure PST: no stn_beam_id={stn_bm_id}"
    stn_beam_dly_src = stn_beams[stn_bm_id]["delay_poly"]
    for idx, pst_bm_id in enumerate(pst_bm_ids):
        assert (
            pst_bm_id in stn_beams[stn_bm_id]["timing_beams"]
        ), f"Error in subarray.configure PST: bad pst_beam_id={pst_bm_id}"
        timing_beam = stn_beams[stn_bm_id]["timing_beams"][pst_bm_id]
        pst_dly_srcs[idx] = timing_beam["pst_poly"]
        jones_srcs[idx] = timing_beam["jones"]

    sig_proc_params = SigProcParams(
        subarray_id,
        stn_bm_id,  # station-beam this FPGA pipile processes
        freq_ids,  # list of frequency IDs this FPGA pipeline is processing
        sstns,  # list of (station, sub-station) tuples
        stn_beam_dly_src,  # station-beam delay poly source
        pst_bm_ids,  # list of pst beam_ids
        pst_dly_srcs,  # list of pst delay poly sources, one per beam_id
        jones_srcs,  # list of pst jones sources, one per beam_id
    )
    return sig_proc_params
