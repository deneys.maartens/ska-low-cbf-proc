# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Code related to checking FPGA registers for values that indicate device
health issues.
"""

import time
from dataclasses import dataclass
from threading import Event, Thread

from ska_control_model import HealthState


@dataclass
class RegisterSpec:
    """Register specific values that reflect on the device health"""

    degraded_delta: int
    """The difference from previous reading which when exceeded indicates
    a degraded performance."""

    failed_delta: int
    """The difference from previous reading which when exceeded indicates
    a device failure."""

    expire: int = 0
    """When non-zero the degraded health will revert to normal after this many
    seconds - unless further deteriation is observed.
    Prevents a one-off glitch to hang around forever.
    """

    value: int = 0  # TODO is this unused?
    """The last value read from a FPGA register."""


class RegisterHealthChecker:
    """Handles checks for DEGRADED/FAILED health conditions based on FPGA
    register values"""

    REGISTER_MAX_VALUE = 0xFFFF_FFFF
    """All register based counters are 32 bit and wrap around (CW)
    For more details see:
    https://jira.skatelescope.org/browse/PERENTIE-1975?focusedId=225782&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-225782
    """

    def __init__(self, spec: RegisterSpec):
        "Set the initial default values"
        self._spec = spec
        self.healthState = HealthState.UNKNOWN
        self.prev_val: int = spec.value
        self.ctime: float = 0.0  # last change time

    def restart(self):
        """Reset health state - when restarting health monitoring"""
        self._spec.value = 0
        self.healthState = HealthState.UNKNOWN
        self.prev_val = self._spec.value
        self.ctime = 0.0

    def __call__(self, new_value: int) -> bool:
        """Perform DEGRADATION/FAILURE checks on register with given value

        :param new_value: a new register value, compared to the previous one
        :return: True if health state changed, False otherwise
        """
        now = time.time()
        diff = new_value - self.prev_val
        # FPGA counters can wrap around and the diff can be negative
        if diff < 0:
            diff += self.REGISTER_MAX_VALUE
        if diff == 0:
            # degraded can self-heal after a timeout (when timeout > 0)
            if (
                self.healthState == HealthState.DEGRADED
                and self._spec.expire > 0
                and now >= self.ctime + self._spec.expire
            ):
                self.healthState = HealthState.OK
                return True
            elif self.healthState == HealthState.UNKNOWN:
                self.healthState = HealthState.OK
                return True
            return False  # nothing changed
        prev_health = self.healthState
        if diff >= self._spec.failed_delta:
            self.healthState = HealthState.FAILED
        elif diff >= self._spec.degraded_delta:
            self.healthState = HealthState.DEGRADED
        # a tolerable change, however if still UNKNOWN flip it to OK
        elif self.healthState == HealthState.UNKNOWN:
            self.healthState = HealthState.OK
        self.prev_val = new_value
        self.ctime = now
        return prev_health != self.healthState


fpga_reg_checks = {
    "corr": {
        # use 'peripheral__regsterName' format as reported in
        # LowCbfProcessor._component_state_changed() 'processor_state' argument
        "lfaadecode100g__non_spead_packet_found": RegisterHealthChecker(
            RegisterSpec(degraded_delta=1, failed_delta=100, expire=60)
        ),
        "lfaadecode100g__novirtualchannelcount": RegisterHealthChecker(
            RegisterSpec(degraded_delta=1000, failed_delta=5000)
        ),
    },
    "pst": {},
    # CNIC is dummy placeholder
    "cnic": {},
    # other peresonalities here
}
"""Describes register values - for each personality and its peripheral - that
affect processor device health"""


class HealthUpdater:
    """
    Class that reads registers (see names above) and from the values in the
    registers determines the overall health of the Processor
    """

    def __init__(self, logger):
        self._logger = logger
        self._core = None
        self._personality = None
        self._thread = None
        self._stop_running = None
        self._poll_secs = 0

        self._next_update_time = None
        self._update_health_state_cbk = None
        self._regs = None
        self._reg_health_state = {}
        self._last_reported_health = None

    def start_health_monitoring(
        self, core, personality, poll_secs, health_attr_update_cbk
    ) -> None:
        """
        Start health monitoring (eg after FPGA loaded)
        :param core: reference to the FPGA object
        :param personality: text eg "corr", "pst" (see fpga_reg_checks keys)
        :param poll_seconds: time between polls of health registers
        :param health_attr_update_cbk: callable function
        """
        self._core = core
        self._personality = personality
        self._poll_secs = poll_secs
        self._update_health_state_cbk = health_attr_update_cbk
        self._regs = fpga_reg_checks[self._personality]
        self._last_reported_health = HealthState.UNKNOWN
        self._thread = Thread(
            target=self._periodic_updater,
            args=(),
        )
        self._stop_running = Event()
        self._thread.start()

    def stop_health_monitoring(self) -> None:
        """Cease health monitoring (eg for FPGA unload)"""
        self._stop_running.set()
        self._thread.join()

    def _periodic_updater(self) -> None:
        """Thread routine to periodically update the health state"""

        self._logger.info(
            "Start monitoring %s registers for health", self._personality
        )

        # Ensure checker for all regs starts in same state
        for reg_checker in self._regs.values():
            reg_checker.restart()

        # initial read of health-related registers
        results = self._read_regs()
        for name, checker in self._regs.items():
            checker.prev_val = results[name]

        # ongoing periodic reading of health-related registers
        self._next_update_time = time.time()  # First check immediately
        while True:
            wait_time = max(0, self._next_update_time - time.time())
            self._stop_running.wait(timeout=wait_time)
            if self._stop_running.is_set():
                break
            self._next_update_time += self._poll_secs

            results = self._read_regs()
            # check each register value
            for name, checker in self._regs.items():
                if checker(results[name]):
                    self._reg_health_state[name] = checker.healthState
            # combine overall health
            self._re_eval_health_state()

        # clean up when health monitoring is stopped
        self._update_health_state_cbk(HealthState.UNKNOWN)
        self._core = None  # essential, or gc in _create_core fails
        self._update_health_state_cbk = None
        self._reg_health_state = {}
        self._stop_running = None
        self._logger.info(
            "Stopped monitoring %s registers for health", self._personality
        )

    def _read_regs(self) -> dict:
        """
        Read all the health-related info from FPGA registers
        :return: dictionary of register values using reg_name as keys
        """
        read_results = {}
        for full_regname in self._regs:
            periph, reg = full_regname.split("__")
            value = self._core[periph][reg].value  # FIXME: may throw
            read_results[full_regname] = value
        return read_results

    def _re_eval_health_state(self) -> None:
        """Re-evaluate overall LowCbfProcessor health state"""
        # OK: 0, DEGRADED: 1, FAILED: 2, UNKNOWN: 3
        # https://developer.skao.int/projects/ska-control-model/en/latest/health_state.html
        # collect all register's health states
        regs_health = self._reg_health_state.values()
        # ignore UNKNOWN health state
        while HealthState.UNKNOWN in regs_health:
            regs_health.remove(HealthState.UNKNOWN)
        if regs_health:
            worst = max(regs_health)
            if self._last_reported_health != worst:
                self._update_health_state_cbk(worst)
                self._last_reported_health = worst
                self._logger.info("Health changed: %s", str(worst))
