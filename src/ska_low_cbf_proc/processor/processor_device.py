# -*- coding: utf-8 -*-
#
# (c) 2020 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
FPGA accelerator card register interface wrapper.
LowCbfProcessor implements a TANGO interface to AlveoCL.
"""
import json
import logging
import os
import time
from threading import Lock  # , Thread

import numpy as np
import tango
import tango.server
from ska_control_model import CommunicationStatus, HealthState, SimulationMode
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import DeviceInitCommand, FastCommand, ResultCode
from ska_tango_base.control_model import AdminMode
from tango.server import attribute, command, device_property

import ska_low_cbf_proc.release as release

from .device_proxy import MccsDeviceProxy

# from .fpga_health_check import fpga_reg_checks
from .processor_component_manager import ProcessorComponentManager

__all__ = ["LowCbfProcessor", "main"]

# numbering subarrays is 1 based, so we'll often use MAX_SUBARRAYS-1 in code
MAX_SUBARRAYS = ProcessorComponentManager.MAX_SUBARRAYS
UNUSED = ProcessorComponentManager.UNUSED
MAX_BEAM_ID = ProcessorComponentManager.MAX_BEAM_ID
MAX_BEAMS = 64  # Max. beams per subarray (for attribute array dimensions)


class LowCbfProcessor(SKABaseDevice):
    """
    Low CBF FPGA control & monitoring TANGO Device.
    """

    def __init__(self, *args, **kwargs):
        # Initialise the Tango device
        self._personality = None
        # list of delay poly statistics, to be published as Tango attribute
        self._stats_delay = []
        # dict of FPGA mode statistics, published as Tango attribute
        self._stats_mode = {
            "ready": False,
            "firmware": None,
        }
        # dict of IO packet statistics, published via Tango attribute
        self._stats_io = {}
        self._extra_mode_info = {}
        self._station_delay_valid = []
        """An array of dictionaries summarising station delay polinomial validity per subarray-beam"""
        # Example:
        # [
        #   {
        #     "subarray_id": 1,
        #     "beams":
        #     [
        #       {
        #         "beam_id": 1,
        #         "valid_delay": True,
        #         "subscription_valid": True
        #       },
        #       {
        #         "beam_id": 2,
        #         "valid_delay": True,
        #         "subscription_valid": True
        #       }
        #     ]
        #  },
        #  {
        #     "subarray_id": 2,
        #     "beams": [ .....

        self._pst_delay_valid = []
        """An array of dictionaries summarising PST delay polinomial validity"""
        # Example:
        #    TBD

        self._subarray_delays_summary = [UNUSED] * MAX_SUBARRAYS
        """This is the top level summary we pass onto Subarray device"""

        self._station_delays_summary = [UNUSED] * MAX_SUBARRAYS
        self._pst_offset_delays_summary = [UNUSED] * MAX_SUBARRAYS
        self._pss_offset_delays_summary = [UNUSED] * MAX_SUBARRAYS
        # Initialise the Tango device
        # note: InitCommand (see below) runs during the call
        super().__init__(*args, **kwargs)

    # Properties (value in database, loaded by helm chart)
    allocator_device = device_property(
        dtype=("DevString",),
        # mandatory=True, # TODO tests fail if this is added, How to add it?
        # TODO FIXME Helm chart should overwrite this default but doesn't
        default_value="low-cbf/allocator/0",
        doc="Tango device with allocation info",
    )

    HEALTH_CLEANUP_PERIOD = 3
    """How often to run health cleanup code (e.g. DEGRADED times out to OK)
    in seconds"""

    # --------------
    # Initialization
    # --------------
    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this device.
        """
        super().init_command_objects()
        self.register_command_object(
            "StartRegisterLog",
            self.StartRegisterLogCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "StopRegisterLog",
            self.StopRegisterLogCommand(tango_device=self, logger=self.logger),
        )

    def create_component_manager(self):
        """
        Create and return a component manager for this device.

        :return: a component manager for this device.
        """
        processor_component_manager = ProcessorComponentManager(
            self.logger,
            self._communication_state_changed,
            self._component_definition_changed,
            self._component_state_changed,
            self.update_mode_stats,
            self.update_delay_stats,
            self.update_io_stats,
            self._update_health_state,
            self.update_subscr_stats,
        )
        return processor_component_manager

    class InitCommand(
        DeviceInitCommand
    ):  # pylint: disable=too-few-public-methods
        """Initialisation command class for this ProcessorDevice."""

        def do(self):  # pylint: disable=arguments-differ
            """Initialises the attributes and properties of ProcessorDevice."""
            self._device._version_id = release.version
            self._device._build_state = (
                f"{release.name}, {release.version}, {release.description}"
            )

            self._device._attribute_quality = tango.AttrQuality.ATTR_INVALID
            self._device._processor_state = {}

            # set initial value of AdminMode (before allocator subscribe)
            # default ONLINE, but if env var is set but invalid use OFFLINE
            admin_env = os.getenv("INITIAL_ADMINMODE", default="online")
            try:
                initial_admin_mode = AdminMode[admin_env.upper()]
            except KeyError:
                initial_admin_mode = AdminMode.OFFLINE  # invalid env var
                self.logger.warning("invalid INITIAL_ADMINMODE %s", admin_env)
            self._device.processor_update_admin_mode(initial_admin_mode)

            # If we have real alveo hardware (with a non-zero serial number),
            # allocator will exist and we can subscribe to allocation events
            serial_no = self._device.get_serialNumber()
            self.logger.info(f"alveo_serial_no = {serial_no}")
            if serial_no != "000000000000":
                self._device.subscribe_to_allocator(
                    self._device.allocator_device
                )
                # since processors are always last to deploy, the allocator
                # should be up and ready to receive command from here
                self._device._register_with_allocator()

            self._device.set_change_event("healthState", True, False)
            self._device.set_change_event("healthStateJson", True, False)
            self._device.set_change_event("simulationMode", True, False)
            self._device.set_change_event("testMode", True, False)
            self._device.set_change_event("stats_delay", True, False)
            self._device.set_change_event("stats_mode", True, False)
            self._device.set_change_event("stats_io", True, False)
            self._device.set_change_event("subarrayDelaysSummary", True, False)

            for att in (
                "serialNumber",
                "subarrayIds",
                "beamIds",
                "stationDelayValid",
            ):
                self._device.set_change_event(att, True, False)
                self._device.set_archive_event(att, True, False)
            self._device._health_state_json = '{ "state": "unknown" }'
            self._device._personality = None
            self._device._health_registers = {}
            self._device._health_reg_loaded: bool = False
            self._device._reg_health_state = {}
            self._device._subarrayIds = []
            self._device._subarray_beams = np.zeros(
                (MAX_SUBARRAYS, MAX_BEAMS), dtype=int
            )
            self._device._lock = Lock()
            self._device._healthState = HealthState.UNKNOWN
            # health monitoring thread
            # self._device._health_thread = Thread(
            #     target=self._device.health_thread
            # )
            # self._device._health_thread.start()
            self._completed()
            message = "LowCbfProcessor init complete"
            self._device.logger.info(message)
            return ResultCode.OK, message

    def _register_with_allocator(self):
        """Call when registering this processor (FSP) with Allocator"""
        assert hasattr(
            self, "_allocator_proxy"
        ), "First run SubscribeToAllocator command"

        self._allocator_proxy.InternalRegisterFsp(
            json.dumps(
                {
                    "serial": [
                        self.get_serialNumber(),
                    ],
                    "fqdn": self.get_name(),
                }
            )
        )

    # -------------------------
    # Tango Command definitions
    # -------------------------
    @command(dtype_in=None, dtype_out=None)
    def Register(self):
        """
        Manually register this Processor==FSP with Allocator
        (This command only used for laptop tests after setting serial number)
        """
        self._register_with_allocator()
        return

    @command(
        dtype_in="DevString",
        doc_in="File Name",
    )
    def StartRegisterLog(self, argin):
        """
        Start register transaction logging.

        :param argin: filename
        """
        command_object = self.get_command_object("StartRegisterLog")
        return command_object(argin)

    @command(dtype_in=None)
    def StopRegisterLog(self):
        """Stop register transaction logging."""
        command_object = self.get_command_object("StopRegisterLog")
        return command_object()

    @command(dtype_in=str, dtype_out=None)
    def SubscribeToAllocator(self, argin):
        """
        Temp method called to cause Processor to subscribe to events from
        a Tango device that has attributes providing allocation info

        :param argin: A string containing a Tango device name. Device must have
        the three "internal_XXX" attributes eg "low-cbf/allocator/0"
        """
        device_name = argin
        self.subscribe_to_allocator(device_name)

    def subscribe_to_allocator(self, tango_device_name) -> None:
        """
        Implementation of subscription to allocation attributes on allocator

        :param tango_device_name: eg 'low-cbf/allocator/0'
        """
        self.logger.info(f"Subscribe alloc events from {tango_device_name}")
        self._allocator_proxy = MccsDeviceProxy(
            tango_device_name, self.logger, connect=False
        )

        alloc_evt_hndlrs = (
            ("internal_alveo", self.component_manager.alloc_alveo_evt),
            ("internal_subarray", self.component_manager.alloc_subarray_evt),
        )
        for attr_name, hndlr in alloc_evt_hndlrs:
            self._allocator_proxy.evt_sub_on_connect(attr_name, hndlr)
        # try:
        #     # self.logger.info("Before subscribe")
        self._allocator_proxy.connect(max_time=120.0)
        # except:
        #     self.logger.info("Connect to allocator failed")

    @command(dtype_in=None, dtype_out=str)
    def gethwdata(self):
        """
        Get register programming information for all configured subarrays
        (as if they were running now, even if they're not) in the same HWDATA
        format that the Model produces
        """
        return self.component_manager.get_hwdata()

    @command(
        dtype_in=str,
        doc_in="JSON dict with name, offset, length fields",
        dtype_out=str,
    )
    def DebugRegRead(self, argin):
        """
        Read a FPGA register, or set of contiguous registers
        :param argin: JSON-encoded dictionary, eg:
            {"name":"reg_name", "offset": 0, "length": 1}
        :return: JSON-encoded list of 32-bit hex register values
        """
        return self.component_manager.read_register(argin)

    @command(
        dtype_in=str,
        doc_in="dict with name, offset, value fields",
        dtype_out=None,
    )
    def DebugRegWrite(self, argin):
        """
        Write a FPGA regoster
        :param argin: JSON-encoded dictionary {"name":"reg_name", "offset": 0, "value": 0}
        """
        self.component_manager.write_register(argin)

    @command(
        dtype_in=str,
        doc_in="dict with hbm number & output directory fields",
        dtype_out=None,
    )
    def DebugDumpHbm(self, argin):
        """
        Capture HBM contents and write to file
        :param argin: JSON dictionary {"hbm": N, "dir": name}
        """
        self.component_manager.dump_hbm(argin)

    @command(
        dtype_in=str,
        doc_in="memory sharing string eg '3Gi:3Gi:3Gi:512Mi:512Mi'",
        dtype_out=None,
    )
    def DebugSetMemConfig(self, argin):
        """
        Set the memory description string used for the FPGA
        Must be done before loading FPGA
        Empty string to undo: then next personality will use "usual" strings
        """
        self.component_manager.set_mem_config(argin)

    # -------------------
    # SKA Command Objects
    # -------------------

    class StartRegisterLogCommand(FastCommand):
        """
        Command class to start register logging
        """

        def __init__(self, tango_device, logger):
            super().__init__(logger=logger)
            self._tango_device = tango_device

        def do(self, argin):
            filename = argin
            self.logger.info(
                "Logging FPGA register transactions to %s", filename
            )
            self._tango_device.component_manager.log_to_file(filename)
            # attempt to prevent register log from reaching Tango logger
            # (doesn't work... upstream bug?)
            self._tango_device.logger.setLevel(logging.INFO)
            for handler in self._tango_device.logger.handlers:
                self.logger.info(f"Setting {handler} to INFO level")
                handler.setLevel(logging.INFO)
                self.logger.info(f"It's now: {handler}")
            return ResultCode.OK, "Started register logging"

    class StopRegisterLogCommand(FastCommand):
        """
        Command class to stop register logging.
        """

        def __init__(self, tango_device, logger):
            super().__init__(logger=logger)
            self._tango_device = tango_device

        def do(self):
            self.logger.info("Ceasing to log FPGA register transactions")
            self._tango_device.component_manager.stop_log_to_file()
            return ResultCode.OK, "Stopped register logging"

    # ------------------
    # Attributes methods
    # ------------------

    # We override SKA-Tango-BASE adminMode attribute to provide the option
    # to disallow adminMode changes while the processor is in use.
    @attribute(dtype=AdminMode, memorized=True, hw_memorized=True)
    def adminMode(self: SKABaseDevice) -> AdminMode:
        """
        Read the Admin Mode of the device.

        :return: Admin Mode of the device
        """
        return self._admin_mode

    @adminMode.write  # type: ignore[no-redef]
    def adminMode(self: SKABaseDevice, value: AdminMode) -> None:
        """
        Set the Admin Mode of the device. Overide of ska-tango-base
        to prevent being set offline while in use

        :param value: requested adminMode of the device.
        """
        if value == self._admin_mode:
            return
        self.processor_update_admin_mode(value)  # processor-specific AdminMode

    def processor_update_admin_mode(self, value):
        """
        Modified adminMode attribute write for LowCBF that is able to
        disallow adminMode change when the Processor FPGA is in use
        """

        if self.component_manager.is_fpga_in_use:
            # adminMode change allowed while FPGA is used?
            env_var = os.getenv(
                "ALLOW_ADMIN_CHANGE_WHILE_USED", default="false"
            )
            if env_var.lower() == "false":
                raise ValueError("Can't change adminMode while FPGA is in use")

        # copy of ska_control_model code block
        if value == AdminMode.NOT_FITTED:
            self.admin_mode_model.perform_action("to_notfitted")
        elif value == AdminMode.OFFLINE:
            self.admin_mode_model.perform_action("to_offline")
            self.component_manager.stop_communicating()
        elif value == AdminMode.MAINTENANCE:
            self.admin_mode_model.perform_action("to_maintenance")
            self.component_manager.start_communicating()
        elif value == AdminMode.ONLINE:
            self.admin_mode_model.perform_action("to_online")
            self.component_manager.start_communicating()
        elif value == AdminMode.RESERVED:
            self.admin_mode_model.perform_action("to_reserved")
        else:
            raise ValueError(f"Unknown adminMode {value}")

        # Tell alveo if it's in an adminMode where it should operate
        on_modes = [AdminMode.ONLINE, AdminMode.MAINTENANCE]
        administratively_enabled = self._admin_mode in on_modes
        self.component_manager.set_fpga_enabled(administratively_enabled)

    @attribute(dtype=SimulationMode)
    def simulationMode(self) -> SimulationMode:
        """
        Simulation mode determined by our underlying FPGA abstraction object
        """
        return SimulationMode(self.component_manager.simulation_mode)

    @attribute(dtype=str)
    def serialNumber(self) -> str:
        """Get the FPGA's serial number (defaults to 12 zeroes)"""
        return self.get_serialNumber()

    @serialNumber.setter
    def serialNumber(self, serial_no: str) -> None:
        """Set Alveo serial number (for testing)"""
        self.component_manager._serial_no = serial_no

    def get_serialNumber(self):
        """non-tango-attribute method to read serial number"""
        return self.component_manager._serial_no

    @attribute(dtype=str)
    def stats_delay(self) -> str:
        """Get internal per-subarray delay polynomial statistics"""
        return json.dumps(self._stats_delay)

    def update_delay_stats(self, new_stats: list) -> None:
        """
        Update attribute with latest subarray statistics
        One list entry for each subarray-beam
        """
        assert isinstance(new_stats, list), "Subarray stats should be list"
        changed = new_stats != self._stats_delay
        self._stats_delay = new_stats
        stats_delay_str = json.dumps(new_stats)
        if changed:
            self.push_change_event("stats_delay", stats_delay_str)
            self._update_subarray_IDs(new_stats)
            self._update_delays_valid(new_stats)

    def update_subscr_stats(self, subscr: dict) -> None:
        """Called when a failure in delay polynomial subscription is detected.
        See FpgaMgrCorr._failed_delay_subscr comments for dictionary structure.
        In the same breath it calculates a summary (delays valid/invalid) for each
        subarray and propagates changes (if any).
        """
        (
            modified,
            new_stats,
            new_summary,
        ) = self.component_manager.update_subscr_stats(
            self._station_delay_valid, subscr, self._station_delays_summary
        )
        if not modified:
            return
        # publish the details to subscribers
        json_str = json.dumps(self._station_delay_valid)
        self.push_change_event("stationDelayValid", json_str)
        self.push_archive_event("stationDelayValid", json_str)
        self._update_subarray_delays_summary(new_summary)

    @attribute(dtype=str, doc="Subarray-Beam delay polynomial validity (JSON)")
    def stationDelayValid(self) -> str:
        """Indicates whether subarray-beam delay polynomials are valid;
        see the comment at self._station_delay_valid definition for the format
        """
        return json.dumps(self._station_delay_valid)

    def _update_delays_valid(self, delay_stats: list) -> None:
        """Re-evaluate and update delay polynomial validity"""
        (
            station_delay_valid,
            station_delays_ok,
        ) = self.component_manager.update_delays_valid(delay_stats)
        if self._station_delay_valid != station_delay_valid:
            self._station_delay_valid = station_delay_valid
            json_str = json.dumps(station_delay_valid)
            self.push_change_event("stationDelayValid", json_str)
            self.push_archive_event("stationDelayValid", json_str)
            self._update_subarray_delays_summary(station_delays_ok)

    @attribute(dtype=str)
    def pstDelayValid(self) -> str:
        """Indicates whether PST offset delay polynomials are valid;
        see the comment at self._pst_delay_valid definition for the format
        """
        return json.dumps(self._pst_delay_valid)

    @attribute(
        dtype=["DevUShort"],
        max_dim_x=MAX_SUBARRAYS,
        doc="Delay validity value per subarray",
    )
    def subarrayDelaysSummary(self) -> list:
        """
        Return an array (one entry for each Subarray) of delay polynomial validity
        indicators for subarray beams processed here. Permitted values are
        0: INVALID
        1: VALID
        2: UNUSED
        This is consumed by LowCbfSubarray device
        """
        return self._subarray_delays_summary

    def _update_subarray_delays_summary(self, value: list) -> None:
        """Update delay validity per subarray"""
        if self._station_delays_summary != value:
            self._station_delays_summary = value
            # what we report to Subarray is a combination of constituent
            # delay validity summaries
            combined_summary = self.component_manager.combined_delays_summary(
                self._station_delays_summary,
                self._pst_offset_delays_summary,
                self._pss_offset_delays_summary,
            )
            if self._subarray_delays_summary != combined_summary:
                self._subarray_delays_summary = combined_summary
                self.push_change_event(
                    "subarrayDelaysSummary", combined_summary
                )

    @attribute(dtype=str)
    def stats_mode(self) -> str:
        """Get fpga mode statistics (eg firmware loaded)"""
        mode_stats = {key: self._stats_mode[key] for key in self._stats_mode}
        mode_stats.update(self._extra_mode_info)
        return json.dumps(mode_stats)

    def update_mode_stats(
        self, fw_name: str, is_ready: bool, extra=None
    ) -> None:
        """
        Update Tango "stats_mode" attribute with latest FPGA mode statistics

        "fw_name" and "is_ready" keys are always present. A dictionary of extra
        key-values may also be published (eg firmware version, build type, etc)
        """
        self._stats_mode["firmware"] = fw_name
        self._stats_mode["ready"] = is_ready
        if isinstance(extra, dict):
            self._extra_mode_info = extra
        else:
            self._extra_mode_info = {}
        mode_stats = self._stats_mode.copy()
        mode_stats.update(self._extra_mode_info)
        values = json.dumps(mode_stats)
        self.push_change_event("stats_mode", values)
        self.push_archive_event("stats_mode", values)

    @attribute(dtype=str)
    def stats_io(self) -> str:
        """Get internal IO statistics - packets in and out"""
        return json.dumps(self._stats_io)

    def update_io_stats(self, new_stats: dict) -> None:
        """
        Update attribute with latest IO statistics
        """
        assert isinstance(new_stats, dict), "IO statistics should be dict"
        self._stats_io = new_stats
        self.push_change_event("stats_io", json.dumps(new_stats))

    # TBD: populate this
    @attribute(dtype=str)
    def healthStateJson(self) -> str:
        """Get the processors health state as JSON string, along the lines of
        {
            "non_spead_packet_found": "DEGRADED"
            "novirtualchannelcount":  "OK"
        }
        """
        return self._health_state_json

    def _read_processor_attribute(self, attr):
        """
        Get value from list and update
        """
        try:
            name = attr.get_name()
        except AttributeError as error:
            self.logger.error(f"attribute error {error}")
            return

        if (
            name not in self._processor_state
            or self._processor_state[name] is None
        ):
            return
        (value, read_time) = self._processor_state[name]
        self.logger.debug(f"updating {name} with: {value} t:{read_time}")
        attr.set_value_date_quality(value, read_time, self._attribute_quality)

    def _write_processor_attribute(self, attr):
        """
        Write a value to the FPGA
        """
        raise ValueError("Use DebugRegWrite to modify registers")

    @attribute(dtype=("DevUShort",), max_dim_x=64)
    def subarrayIds(self):
        """
        Subarrays handled by this processor.
        """
        return self._subarrayIds

    @attribute(dtype=((int,),), max_dim_y=MAX_SUBARRAYS, max_dim_x=MAX_BEAMS)
    def beamIds(self) -> np.array((MAX_SUBARRAYS, MAX_BEAMS), dtype=int):
        """
        Subarray beams handled by this processor.
        """
        return self._subarray_beams

    def _update_subarray_IDs(self, delay_stats):
        """Update the lists of subarrays and beams.
        Notify subscribers in case of any change.
        """
        parent_subarrays = []
        beams = np.zeros((MAX_SUBARRAYS, MAX_BEAMS), dtype=int)
        for entry in delay_stats:
            if "subarray_id" not in entry:
                continue
            subarray_id = entry["subarray_id"]
            parent_subarrays.append(subarray_id)

            if subarray_id < 1 or subarray_id > MAX_SUBARRAYS:
                self.logger.error(
                    f"subarray {subarray_id} is out of range [1, {MAX_SUBARRAYS}]"
                )
                continue
            if "beams" not in entry:
                continue
            # collect this subarray's beams
            for i, bm in enumerate(entry["beams"]):
                if i < MAX_BEAMS:
                    beams[subarray_id - 1][i] = bm["beam_id"]
                else:
                    self.logger.error(
                        f"MAX_BEAMS ({MAX_BEAMS}) number exceeded: {i}"
                    )

        parent_subarrays = sorted(parent_subarrays)
        if self._subarrayIds != parent_subarrays:
            self.logger.info("SUBARRAYS CHANGED")
            self._subarrayIds = parent_subarrays
            self.push_change_event("subarrayIds", parent_subarrays)
            self.push_archive_event("subarrayIds", parent_subarrays)

        if not np.array_equal(self._subarray_beams, beams):
            self.logger.info("BEAMS CHANGED")
            self._subarray_beams = beams
            self.push_change_event("beamIds", beams)
            self.push_archive_event("beamIds", beams)

    # ----------
    # Callbacks
    # ----------
    def _communication_state_changed(self, communication_state):
        if communication_state == CommunicationStatus.ESTABLISHED:
            self._attribute_quality = tango.AttrQuality.ATTR_VALID
        else:
            self._attribute_quality = tango.AttrQuality.ATTR_INVALID
        super()._communication_state_changed(communication_state)

    def _component_definition_changed(self, definition):
        # get a list of known attributes (they may not have been initialised)
        my_attributes = self.get_polled_attr()
        for attribute_name in self._processor_state:
            if attribute_name in my_attributes:
                self.remove_attribute(attribute_name)
        self._processor_state.clear()

        # -- # FIXME force temperature
        # -- definition.update( { "alveo_temperature":
        # --                      {
        # --                          "write": None,
        # --                          "type": str,
        # --                          "length": 1,
        # --                      }
        # --                     } )

        for attribute_name, attribute_data in definition.items():
            self._processor_state[attribute_name] = None
            access = (
                tango.AttrWriteType.READ_WRITE
                if attribute_data["write"]
                else tango.AttrWriteType.READ
            )
            data_type = (
                (attribute_data["type"],)
                if attribute_data["length"] > 1
                else attribute_data["type"]
            )

            attr = tango.server.attribute(
                name=attribute_name,
                dtype=data_type,
                access=access,
                label=attribute_name,
                max_dim_x=attribute_data["length"],
                fread="_read_processor_attribute",
                fwrite="_write_processor_attribute",
            ).to_attr()
            attr.set_archive_event(True, False)

            # TODO: Creating hundreds of attributes at once is killing our
            # throughput. It is common to encounter "failed to obtain monitor
            # lock" DevFailed here. We might need to re-think this.
            attempts = 3
            for attempt in range(1, attempts + 1):
                try:
                    self.add_attribute(
                        attr,
                        self._read_processor_attribute,
                        self._write_processor_attribute,
                        None,
                    )
                    self.set_change_event(attribute_name, True, False)
                    break
                except tango.DevFailed as fail:
                    self.logger.error(
                        f"Failed to add {attribute_name}: {repr(fail)}"
                    )
                    time.sleep(1)
                    if attempt == attempts:
                        raise
        if self._personality != self.component_manager.personality:
            self.logger.info("PERSONALITY CHANGED")
            self._personality = self.component_manager.personality
            # self._health_registers = fpga_reg_checks[self._personality.lower()]
            # self._health_reg_loaded = False

    def _component_state_changed(
        self, fault=None, power=None, poll_time=None, processor_state=None
    ):
        super()._component_state_changed(fault=fault, power=power)

        if not processor_state:
            return

        for name, value in processor_state.items():
            changed = (
                self._processor_state[name] is None
                or self._processor_state[name][0] != value
            )
            val = processor_state[name]
            self._processor_state[name] = (val, poll_time)
            if changed:
                self.push_change_event(name, val)
                self.push_archive_event(name, val)
        # self._check_health(processor_state)

    # def _load_health_registers(self, processor_state):
    #     """Initial snapshot of register values - this is the baseline"""
    #     self.logger.info("LOADING _health_registers")
    #     regs = self._health_registers
    #     with self._lock:
    #         for name, value in processor_state.items():
    #             if name in regs:
    #                 regs[name].prev_val = value
    #         self._health_reg_loaded = True

    # def _check_health(self, processor_state):
    #     """Check if the change in processor_state has an effect on
    #     LowCbfProcessor heathState"""
    #     # 1st time around just load the register values and wait for
    #     # further changes
    #     if not self._health_reg_loaded:
    #         self._load_health_registers(processor_state)
    #         return
    #     log = self.logger
    #     regs = self._health_registers
    #     log.info(f"CHECKING HEALTH {self._personality}")
    #     with self._lock:
    #         for name, value in processor_state.items():
    #             if name in regs:
    #                 check = regs[name]
    #                 if check(value):
    #                     log.info(f"{name} changed to {check.healthState}")
    #                     self._reg_health_state[name] = check.healthState
    #         self._re_eval_health_state()

    # def health_thread(self):
    #     """A simple health cleanup thread, transitions DEGRADED registers into
    #     OK after a timeout and updates the overall LowCbfProcessor heath state
    #     accordingly"""
    #     regs_health = self._reg_health_state
    #     while True:
    #         time.sleep(self.HEALTH_CLEANUP_PERIOD)
    #         with self._lock:
    #             regs = self._health_registers
    #             for name, check in regs.items():
    #                 if check(check.prev_val):
    #                     self.logger.info(f"{name} CHANGED {check.healthState}")
    #                     regs_health[name] = check.healthState
    #             self._re_eval_health_state()

    # def _re_eval_health_state(self):
    #     """Re-evaluate overall LowCbfProcessor health state"""
    #     # OK: 0, DEGRADED: 1, FAILED: 2, UNKNOWN: 3
    #     # https://developer.skao.int/projects/ska-control-model/en/latest/health_state.html
    #     # collect all register's health states
    #     regs_health = self._reg_health_state.values()
    #     # ignore UNKNOWN health state
    #     while HealthState.UNKNOWN in regs_health:
    #         regs_health.remove(HealthState.UNKNOWN)
    #     if regs_health:
    #         worst = max(regs_health)
    #         if worst != self._health_state:
    #             self._update_health_state(worst)


# ----------
# Run server
# ----------
def main(args=None, **kwargs):
    """Main function of the LowCbfProcessor module."""
    return tango.server.run((LowCbfProcessor,), args=args, **kwargs)


if __name__ == "__main__":
    main()
