"""
Test handling of FPGA registers that affect device's health state
"""

from time import sleep

import pytest
from ska_control_model import HealthState

from ska_low_cbf_proc.processor.fpga_health_check import (
    RegisterHealthChecker,
    RegisterSpec,
)

DEGRADED_COUNT = 2
FAILED_COUNT = 50
AUTO_HEAL_TIMEOUT = 1


@pytest.fixture
def checker():
    return RegisterHealthChecker(
        RegisterSpec(
            degraded_delta=DEGRADED_COUNT,
            failed_delta=FAILED_COUNT,
            expire=AUTO_HEAL_TIMEOUT,
        )
    )


@pytest.fixture
def fast_checker():
    """Used to test 32-bit FPGA register wrap-around"""
    return RegisterHealthChecker(
        RegisterSpec(
            degraded_delta=0x1_0000,
            failed_delta=0x2_0000,
            expire=AUTO_HEAL_TIMEOUT,
        )
    )


class TestRegisterChecker:
    def test_initial_state(self, checker):
        """"""
        assert checker.healthState == HealthState.UNKNOWN
        assert checker.prev_val == 0
        assert checker.ctime == 0.0

    def test_health_ok(self, checker):
        """Check the health state transitions from UNKNOWN to OK when there's
        no change in register value"""
        assert checker.healthState == HealthState.UNKNOWN
        # apply the same register value
        assert checker(checker.prev_val)
        assert checker.healthState == HealthState.OK

    def test_small_change(self, checker):
        """Check the health state does no DEGRADE when a change is below
        the threshold
        """
        # ensure the threshold value will allow us to conduct the test
        assert checker._spec.degraded_delta > 1

        assert checker.healthState == HealthState.UNKNOWN
        # counter increment just below the threshold
        assert checker(DEGRADED_COUNT - 1)
        assert checker.healthState == HealthState.OK

    def test_health_degraded(self, checker):
        """Check the health state goes to DEGRADED when a change is above
        the threshold
        """
        assert checker.healthState == HealthState.UNKNOWN
        assert checker(DEGRADED_COUNT)
        assert checker.healthState == HealthState.DEGRADED

    def test_degraded_autoheals(self, checker):
        """Check the health state reverts from DEGRADED to OK after a
        timeout"""
        assert checker.healthState == HealthState.UNKNOWN
        assert checker(DEGRADED_COUNT)
        assert checker.healthState == HealthState.DEGRADED
        sleep(AUTO_HEAL_TIMEOUT + 0.2)
        # call checker with the unchanged register value
        assert checker(DEGRADED_COUNT)
        assert checker.healthState == HealthState.OK

    def test_no_quick_autoheal(self, checker):
        """Check the health state won't revert from DEGRADED to OK before
        the timeout expires"""
        assert checker.healthState == HealthState.UNKNOWN
        assert checker(DEGRADED_COUNT)
        assert checker.healthState == HealthState.DEGRADED
        sleep(AUTO_HEAL_TIMEOUT - 0.2)
        # call checker with the unchanged register value
        assert not checker(DEGRADED_COUNT)
        assert checker.healthState == HealthState.DEGRADED

    def test_will_not_autoheal(self, checker):
        """Check the health state won't revert from DEGRADED to OK after a
        timeout when the new change is above threshold"""
        assert checker.healthState == HealthState.UNKNOWN
        reg_value = DEGRADED_COUNT
        checker(reg_value)
        assert checker.healthState == HealthState.DEGRADED
        sleep(AUTO_HEAL_TIMEOUT + 0.2)
        # call checker with another value which would keep it's state degraded
        assert not checker(reg_value * 2)
        assert checker.healthState == HealthState.DEGRADED

    def test_failed_from_unknown(self, checker):
        """Check the health state goes from UNKNOWN to FAILED when the change
        is above the corresponding threshold"""
        assert checker.healthState == HealthState.UNKNOWN
        assert checker(FAILED_COUNT)
        assert checker.healthState == HealthState.FAILED

    def test_failed_from_ok(self, checker):
        """Check the health state goes from OK to FAILED when the change is
        above the corresponding threshold"""
        assert checker.healthState == HealthState.UNKNOWN
        assert checker(checker.prev_val)
        assert checker.healthState == HealthState.OK
        assert checker(checker.prev_val + FAILED_COUNT)
        assert checker.healthState == HealthState.FAILED

    def test_failed_from_degraded(self, checker):
        """Check the health state goes from DEGRADED to FAILED when the change is
        above the corresponding threshold"""
        assert checker.healthState == HealthState.UNKNOWN
        assert checker(DEGRADED_COUNT)
        assert checker.healthState == HealthState.DEGRADED
        assert checker(checker.prev_val + FAILED_COUNT)
        assert checker.healthState == HealthState.FAILED

    def test_failed_will_not_autoheal(self, checker):
        """Check the health state stays FAILED after 'auto-heal' timeout"""
        assert checker.healthState == HealthState.UNKNOWN
        reg_value = FAILED_COUNT
        checker(reg_value)
        assert checker.healthState == HealthState.FAILED
        sleep(AUTO_HEAL_TIMEOUT + 0.2)
        # register value does not change but it won't autoheal
        checker(reg_value)
        assert checker.healthState == HealthState.FAILED

    def test_register_wrap_around(self, fast_checker):
        """Check handling of FPGA register counter  wrapping around"""
        assert fast_checker.healthState == HealthState.UNKNOWN
        reg_value = 0
        # fast-forward to the threshold of register wrap-around: 0xFFFF_FFFF
        while reg_value <= RegisterHealthChecker.REGISTER_MAX_VALUE:
            fast_checker(reg_value)
            assert fast_checker.healthState == HealthState.OK
            reg_value += 0xFFFF

        assert fast_checker.healthState == HealthState.OK
        # reduce the DEGRADED threshold ...
        fast_checker._spec.degraded_delta = 2
        # ... and check that register counter incremented by 1 didn't trip
        # the health state change
        assert not fast_checker(0)
